import re

def file_to_list(file_path):
    f = open(file_path, "r")
    return f.read().split("\n") 

def split_str_by_steps(str_input,steps):
    res = []
    for i in range(0,len(str_input),steps):
        splitted_str = ""
        for j in range(steps):
            try:
                splitted_str += str_input[i+j]
            except IndexError:
                splitted_str += " "
        res += [splitted_str]
    return res


STACK_WIDTH = 9
STACK_HEIGHT = 8


input_text = file_to_list("input")

stack = input_text[:STACK_HEIGHT]
instructions = input_text[STACK_HEIGHT + 2:]

stack = list(map(lambda x : split_str_by_steps(x, 4), stack))
stack = list(map(lambda x : list(map(lambda y : y.strip().strip("[").strip("]"), x)), stack))

instructions = list(map(lambda x : re.split("\D+",x),instructions))
instructions = list(map(lambda x : x[1:], instructions))
instructions = list(map(lambda x : list(map(lambda y : int(y), x)), instructions))

print(stack)
print(instructions)

