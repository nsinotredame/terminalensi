# Projets
## Jeu de la vie
### Présentation
Je vous propose un mini projet individuel visant à reproduire le jeu de la vie proposé par Conway en 1970. Le jeu de la vie est ce qu'on appelle un automate cellulaire : c'est un « tableau » dont l'état de chaque case ou cellule dépent de l'état des cases voisines. À chaque « tour » de jeu, on met à jour l'ensemble des cases en fonction de leurs voisines et on recommence.

Dans le jeu de la vie, chaque cellule peut avoir deux états :

- vivante
- morte

L'état d'une cellule au tour suivant dépend de l'état de ses huit voisins directs. Il y a deux règles simples qui s'appliquent :

- une cellule morte possédant exactement trois voisines vivantes devient vivante : elle naît
- une cellule vivante possédant deux ou trois voisines vivantes le reste, sinon elle meurt

Si vous voulez des précisions ou en savoir plus allez sur la [page Wikipédia du jeu de la vie](https://fr.wikipedia.org/wiki/Jeu_de_la_vie)

Ou bien en regardant cette super vidéo: 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/S-W0NX97DB0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Travail demandé

Vous allez donc coder le jeu de la vie en utilisant la programmation orientée objet. Comme c'est le premier projet de l'année, je vais beaucoup vous guider. 

!!! info "Classe JeuDeLaVie"
    Vous allez créer une classe JeuDeLaVie avec un attribut tableau. Nous auront besoin de quelques modules, voici donc comment devra commencer votre programme : 
    ```python
    import os
    import time
    import copy

    class JeuDeLaVie:    
    ```

!!! info "Constructeur"
    Le constructeur prendra en argument un tableau à deux dimensions de taille quelconque rempli de 0 et de 1 représentant les cellules. Un 0 pour une cellule morte et un 1 pour une cellule vivante. Voici les caractéristiques attendues du constructeur : 
    ```python
    def __init__(self, tableau):
            """
            Affecte un tableau à deux dimensions à l’attribut tableau

            :param tableau: tableau à deux dimensions
            """
    ```

!!! info "Méthode `affiche()`"
    Cette méthode doit permettre d'afficher le tableau dans le shell. On n'utilisera pas d'interface graphique pour simplifier le programme. Attention, il faudra effacer le contenu du shell avant chaque affichage pour donner l'effet d'une vidéo. Après avoir importé le module os il faut utiliser l'une des commandes suivantes en fonction de votre système d'exploitation :
    - pour Windows : `os.system('cls')`
    - pour Linux : `os.system('clear')`

!!! info "Autres méthodes"
    Voici la liste des autres méthodes de la classe JeuDeLaVie avec leurs docstrings qui font office de cahier des charges.

    ```python
    def run(self, nombre_tours, delai):
            """
            Méthode principale du jeu.

            Fait tourner le jeu de la vie pendant nombre_tours.
            Elle rafraichit l’affichage à chaque tour
            et attend delai entre chaque tour.

            :param nombre_tours: nombre de tours à effectuer
            :param delai: temps d’attente en secondes entre chaque tour
            """
    ```

    ```python
    def tour(self):
            """
            Met à jour toute les cellules du tableau en respectant les règles
            du jeu de la vie.
            """
    ```

    ```python
    def valeur_case(self, i, j):
            """
                Renvoie la valeur de la case [i][j] ou 0 si la case n’existe pas.
            """
    ```
    
    !!! warning "Attention aux dépassements de liste"
        Je vérifie si je regarde le voisin à la coordonnée:
        
        - i-1 ou j-1 -> que i != 0 ou  j != 0
        - j+1 ou j+1 -> que i != len(tableau)-1 ou j != len(tableau[0])-1
        
    ```python
    def total_voisins(self, i, j):
            """Renvoie la somme des valeurs des voisins de la case [i][j]."""
    ```

    ```python
    def resultat(self, valeur_case, total_voisins):
            """
            Renvoie la valeur suivante d’une la cellule.

            :param valeur_case: la valeur de la cellule (0 ou 1)
            :param total_voisins: la somme des valeurs des voisins
            :return: la valeur de la cellule au tour suivant
    ```

!!! info "Utilisation du programme"
    Pour instancier le jeux de la vie et la lancer il faudra alors ajouter ces commandes à la fin de votre fichier : 

    ```python
    tableau =  [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]

    mon_jeu = JeuDeLaVie(tableau)
    mon_jeu.run(100, 0.1)
    ```
    Et pour avoir un affichage correct il sera surement nécessaire de lancer le programme à partir de la ligne de commande :

    `python3 JeuDeLaVie.py`

    L'état initial donné ici devrait faire apparaitre un motif qui se déplace en diagonale. Voici un autre état initial qui provoque une expension des cellules vivantes :

    ```python
    tableau =  [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
    ```

    À vous d'essayer différentes situations initiales pour voir ce qu'il se passe. 

### Aides et conseils

!!! tip "Aides et conseils"
    **Délais**

    Pour faire attendre le programme nous avons besoin du module `time` avec la méthode `time.sleep()`.

    **Copier un tableau**

    À un moment donné, vous allez avoir besoin de copier l'attribut tableau de la classe. Pour avoir une copie indépendante de l'original il vous faudra utiliser la méthode `copy.deepcopy()` du module `copy`. 

!!! tip "Aide pygame"
    Code nécessaire pour faire une interface graphique et dessiner des rectangles/carrés avec pygame:

    ```python
    import pygame

    # Initialisation de Pygame
    pygame.init()

    # Initialisation de la fenêtre d'affichage
    fenetre = pygame.display.set_mode((largeur,hauteur))

    #Dessiner un carré/rectangle vide (avec une bordure)
    pygame.draw.rect(fenetre, couleur, (coordonnée_gauche,coordonnée_droite,largeur,hauteur), 1)

    #Dessiner un carré/rectangle plein
    pygame.draw.rect(fenetre, couleur, (coordonnée_gauche,coordonnée_droite,largeur,hauteur), 0)

    #Actualiser l'affichage
    pygame.display.flip()
    ```

!!! tip "Jeux de test"
    A executer pour voir si vos méthodes marchent bien
    ```python
    petit_tableau = [[0,0,0],[1,1,1],[0,0,0]]
    petit_tableau2 = [[0,1,0],[0,1,0],[0,1,0]]

    jeu = JeuDeLaVie(petit_tableau)
    assert(jeu.valeur_case(0, 1) == 0)
    assert(jeu.valeur_case(1,0) == 1)
    assert(jeu.total_voisins(0, 1) == 3)
    assert(jeu.resultat(jeu.valeur_case(0, 1),jeu.total_voisins(0, 1)) == 1)

    jeu2 = JeuDeLaVie(petit_tableau2)
    assert(jeu2.valeur_case(0, 1) == 1)
    assert(jeu2.valeur_case(1, 0) == 0)
    assert(jeu2.total_voisins(1, 0) == 3)
    assert(jeu2.resultat(jeu2.valeur_case(1, 0),jeu2.total_voisins(1, 0)) == 1)
    assert(jeu2.resultat(jeu2.valeur_case(0, 1),jeu2.total_voisins(0, 1)) == 0)
    jeu2.tour()
    assert(jeu2.valeur_case(0, 1) == 0)
    assert(jeu2.valeur_case(1,0) == 1)
    ```

!!! info "Améliorations"
    Si vous êtes rapide, vous pouvez apporter des améliorations au programme. Voici quelques suggestions :

    - détecter s'il n'y a plus de changements dans le tableau entre deux tours et arrêter alors le programme
    - permettre de choisir le symbole représentant une cellule vivante
    - proposer de choisir parmi des configurations - initiales pré-enregistrées
    ajouter une interface graphique (par exemple avec pygame)
    - toute autre idée pour améliorer le programme…

!!! warning "Propreté du code"
    Nous verrons dans ce projet et au cours de l'année, l'importante des **commentaires**.

    Les commentaires sont des sections de code qui ne sont pas exécutées par python, elles servent à donner des informations à la personne (humaine) qui va lire le programme ensuite.

    C'est très **important** notemment pour le travail en équipe, afin que votre code soit lisible par une autre personne, de la même manière que vous soignez votre orthographe et votre écriture manuscrite.

    Même pour vous c'est important afin de reprendre votre code plus tard.

    ![](20221004125823.png){: .center width=30%}

    La propreté du code ne dépend pas que des commentaires, les noms de variables doivent être explicites, par exemple une variable servant à stocker un maximum doit s'appeller `maximum` et non `a`

!!! warning "Plagiat" 
    Je sais que vous allez trouver du code sur internet, d'autres personnes ont même déjà surement implémenté ce projet en python.

    Le plagiat n'existe pas vraiment en informatique tant que du code est open source, néanmoins pour ce projet, je veux **VOTRE** version du code.

    Si vous copiez/collez du code de quelque part sur internet, google existe, ça se retrouve. Vous avez bien sur le droit de vous inspirer, mais il faut alors savoir expliquer ce que vous avez pris.

    Ce que je ne veux pas voir : 

    ![](20221004131627.png){: .center width=30%}

### Tableau du barème

Le code est noté sur 14 points, selon le tableau que voici:

!!! info "Barème"
    | Tâche | Barème |
    |---------|-------------|
    | Affichage basique | 1 point |
    | Affichage amélioré | 1 point |
    | Valeur case | 1 point |
    | Total voisins | 2 points |
    | Tour | 2 point |
    | Run | 1 point |
    | Arrêt automatique | 1 point |
    | Configurations pré-enregistrées | 1 point |
    | Interface graphique (bonus) | 2 points bonus |
    | Code propre | 2 points |
    | Commentaires | 2 points |

Le reste de la note sera déterminé par un entretien individuel dans lequel vous devrez m'expliquer votre code, je pourrais aussi vous poser des questions sur votre code.

## Calculatrice NPI

Dans ce projet par groupes de 2 ou 3, vous allez créer une calculatrice avec une interface graphique à l'aide du module tkinter. Cette calculatrice doit respecter la notation polonaise inversée.

!!! info "Répartition des tâches"
    Il s'agit de votre premier travail en groupe, vous devrez donc vous répartir le travail, il y a 3 modules distincts à créer. Pour mettre en commun votre travail vous pouvez utiliser le moyen de votre choix (clé usb, cloud, etc...) ou bien git, en vous créant un compte gitlab ou github et en installant le logiciel git sur vos machines.

### Travail à effectuer

Chaque module correspond à un fichier python distinct, pour importer un module qui s'appelle `mon_module.py` sur un fichier dans le même dossier, on fait `import mon_module` au début du fichier.

!!! info "Module interface"
    Pour l'interface il vous faut:

    - Une zone de texte, où seront affichés les nombres saisis ainsi que le résultat
    - Un bouton pour chaque chiffre (0-9)
    - Un bouton pour le résultat (=)
    - Un bouton pour espacer les chiffres et les opérateurs
    - Un bouton pour effacer tout le calcul

    Chaque bouton doit faire appel à la fonction correspondante

!!! info "Module pile"
    Dans ce module vous implémenterez la classe Pile, que nous avons déjà surement fait auparavant, il faut que cette classe implémente toutes les opérations de la classe Pile

!!! info "Module NPI"
    C'est le code qui gère le fonctionnement de la calculatrice en elle même, il faut gérer:

    - La lecture d'un nombre
    - La lecture d'un opérateur
    - La lecture d'un calcul entier

### Aide et conseils

[Exercice bac sur la notation polonaise inversée](exercice_bac_piles2.pdf)
[Activité découverte tkinter](nsi_decouverte_tkinter.pdf)

### Barème

Voici le barème pour ce projet, comme le projet précédant le rendu du code sera suivi d'un petit entretien, cette fois ci par groupe

!!! info "Barème"

    Interface:

    - Saisie des nombres et operateurs 2
    - Séparation des nombres par la touche espace 1
    - Fonction clear 1
    - Fonction evaluer 1

    Total : 5

    Pile:

    - 1 point par méthode implémentée 3
    - 1 point pour les tests 1

    Total : 4

    NPI:
    
    - Lecture d'un nombre 2
    - Lecture d'un opérateur 2
    - Lecture de la chaine complete 1

    Total : 5

    - Commentaires: 1
 
## Carnet d'adresses 

### Présentation 

!!! abstract "Présentation"
    L'objectif de ce projet est de créer un répertoire contenant les noms, prénoms et numéros de téléphone de personnes. Pour ne pas trop le complexifier, le programme fonctionnera sans interface graphique avec un menu permettant de choisir des actions comme celui-ci : 

    ```
    Que voulez-vous faire ?
    1 - Ajouter un contact
    2 - Supprimmer un contact
    3 - Afficher tous les contacts
    4 - Chercher un contact
    5 - Quitter
    Choix :
    ```

    Vous utiliserez obligatoirement la programmation orientée objet pour réaliser ce projet. C'est à dire que vous devez avoir une classe `Repertoire` et une classe `Contact`. La classe `Repertoire` aura une méthode `run()` (parmi d'autres) qui lancera le programme. 

    Pour l'enregistrement des contacts dans la base de données vous utiliserez SQLite en python.

### Cahier des charges

!!! abstract "Cahier des charges"
    Voici les fonctionalités que votre projet doit implémenter :

    - un menu (identique à celui ci-dessus) pour choisir les différentes actions
    - ajout d'un contact ;
    - suppression d'un contact avec demande de confirmation (trouvez une façon simple de le faire) ;
    - affichage de tous les contacts par ordre alphabétique des noms puis des prénoms ;
    - recherche parmis les contacts (nom et prénom) ;
    - quitter l'application
    - sauvegarde des contacts dans une base de données et chargement depuis une base de données au démarrage de l'application

    Il vous est demandé de bien utiliser les fonctionalités de la programmation orientée objet.

    Pour commencer, vous pouvez créer la classe Contact avec une méthode affiche(). Puis vous pouvez vous attaquer à la classe Repertoire qui devra stocker une liste de contacts. 

### Aide 


!!! abstract "Aide SQL"

    Voici une liste de commandes de SQLite3 qui vous seront utiles. 

    ```python
    import sqlite3

    # Connection à la base de données
    conn = sqlite3.connect('data.db')

    # Création du curseur qui permet d'interagir avec la base
    cur = conn.cursor()

    # Envoi d'une requête SQL
    cur.execute(string_commande_mysql)

    # Exemple avec la requête d'insertion
    cur.execute("INSERT INTO table (attribut1, attribut2) VALUES (?,?)", (variable1, variable2))

    # Pour récupérer une ligne de la requête (peut être utilisée plusieurs fois)
    # La ligne est un tuple
    ligne = cur.fetchone()

    # Pour récupérer toutes les lignes de la requête
    # C'est une liste de tuples
    lignes = cur.fetchall()

    # Pour appliquer les modifications (après un INSERT ou UPDATE)
    conn.commit()


    ```

    Créer une table en SQL : 

    ```SQL
    CREATE TABLE person (
                  attribut TYPE,
                  attribut2 TYPE,
                  attribut3 TYPE
           );
    ```

    Types de SQLITE : `INTEGER`, `TEXT`, et autres pas forcéments utiles pour ce projet.

    Pour définir une clé primaire : `PRIMARY KEY` après l'attribut.

    Pensez à vous créer un fichier pour initialiser la base de données avec les tables, par exemple un fichier `init_db.py`, que vous lancerez **une fois** ou plusieurs fois si vous voulez réinitialiser votre base de données.

### Barème

!!! info "Barème"
    Voici le barème pour ce projet:

    - Code : **15 points**, dont:

        - Ajout d'un contact - **3points**

        - Suppression d'un contact - **3 points**

        - Affichage des contacts - **1 point**

        - Rechercher un contact - **2 points**

        - Quitter - **1 point**

        - Interface - **3 points**

        - Commentaires - **2 points**

    - Entretien/Oral : **5 points**

### Pour aller plus loin

!!! example "Pour aller plus loin"

    Si vous voulez améliorer votre programme une fois fini vous pouvez:

    - Rajouter une interface graphique (avec Tkinter par exemple)

    - Ajouter une fonctionnalité permettant d'exporter des contacts au format [Vcard](https://fr.wikipedia.org/wiki/VCard)