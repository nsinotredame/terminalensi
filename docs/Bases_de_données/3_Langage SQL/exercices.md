
# Exercices

!!! example "Exercice 10"

    Soit la base de données d’un festival de musique : dans une représentation peut participer un ou plusieurs musiciens. Un musicien ne peut participer qu’à une seule représentation.

    -  **Representation** (<u>numRep</u> , titreRep , lieu)
    -  **Musicien** (<u>numMus</u> , nom , #numRep)
    -  **Programmer** (<u>Date</u> , #numRep, tarif)

    Indiquez les requêtes qui permettent d’obtenir :

    1. La liste des titres des représentations.
    2. La liste des titres des représentations ayant lieu au "théâtre allissa".
    3. La liste des noms des musiciens et des titres des représentations auxquelles ils participent.
    4. La liste des titres des représentations, les lieux et les tarifs du 25/07/2008.
    5. Le nombre des musiciens qui participent à la représentations n°20.
    6. Les représentations et leurs dates dont le tarif ne dépasse pas 20 euros.

!!! example "Exercice 11"

    Soit le modèle relationnel suivant relatif à la gestion des locations de ﬁlms :

    -  **Clients** (<u>codecli</u>, prenomcli, nomcli, ruecli, cpcli, villecli)
    -  **Films** (<u>codeﬁlm</u>, nomﬁlm)
    -  **Locations** (<u># codecli</u>, <u># codeﬁlm</u>, datedebut, duree)

    Indiquez les requêtes qui permettent d’obtenir :

    1. L’insertion du ﬁlm "The Raid" avec le code numéro 12.
    2. L’insertion de client numéro 124 qui s’appelle Jean Talu (les autres informations sur ce client ne sont pas connues).
    3. La suppression du ﬁlm "Dans la peau de John Malkovitch".
    4. Le renommage du ﬁlm "Boulevard de la Mort" en "Death Proof".

!!! example "Exercice 12"

    Soit la Base de données **hôtel** qui contient 3 relations :

    -  **Chambre** (<u>numChambre</u>, prix, nbrLit, nbrPers, confort, equipement)
    -  **Client** (<u>numClient</u>, nom, prenom, adresse)
    -  **Reservation** (<u># numClient, # numChambre</u>, dateArr, dateDep)

    Le contenu de chaque relation est :

    ![](../Images/tab14.JPG)

    Indiquez les requêtes qui permettent d’obtenir :

    1. Les numéros de chambres avec TV.
    2. Les numéros de chambres et leurs capacités.
    3. La capacité théorique d’accueil de l’hôtel.
    4. Le prix par personne des chambres avec TV.
    5. Les numéros des chambres et le numéro des clients ayant réservé des chambres pour le 09/02/2004.
    6. Les numéros des chambres coûtant au maximum 80 Euro, ou ayant un bain et valant au maximum 120 Euro.
    7. Les Nom, Prénoms et adresses des clients dans le noms commencent par « D ».
    8. Le nombre de chambres dont le prix est entre 85 et 120 Euro.
    9. Les noms des clients n’ayant pas ﬁxé la date de départ.
