# Le langage SQL

### Les requêtes d'interrogation

#### Construction d'une requête simple

Le langage SQL (Strutured Query Language) permet d’interroger les bases de données en traduisant les opérations par des mots-clés simples. C’est un langage normalisé.

!!! note "Remarque"
    Le langage SQL n’est pas sensible à la casse. néanmoins, il est coutumier d’écrire les mots-clés en majuscule.

!!! abstract "Définition"
    L’instruction de base pour l’interrogation d’une base de données en SQL est constituée du mot-clé `SELECT` suivi du mot-clé `FROM`.

      - `SELECT` permet de sélectionner les attributs dont il faut aﬃcher les valeurs. Le caractère $*$ permet d’aﬃcher les valeurs de tous les attributs.
      -  `FROM` permet de sélectionner la relation à explorer.

    Une requête SQL ﬁnit toujours par un point-virgule `;`

*Exemple*

On considère la relation **LIVRES** ( <u>id</u> , titre, auteur, ann_publi)

![](../Images/tab11.JPG)

Elle contient les éléments suivants :

![](../Images/tab12.JPG)

- Affichage de tous les titres présents dans la relation LIVRES

```sql
SELECT  titre 
FROM  livres;
```
renvoie l’affichage :

```
1984
Dune
Fondation
Farenheit  451
Chroniques  martiennes
```

- Affichage de tous les titres et années de publication présents dans la relation LIVRES

```sql
SELECT  titre,  ann_publi 
FROM  livres;
```
renvoie l’aﬃchage :

```
1984,  1949
Dune,  1965
Fondation,  1951
Farenheit  451,  1953
Chroniques  martiennes,  1950
```

- Affichage de tous les attributs présents dans la relation LIVRES

```sql
SELECT  *
FROM  livres;
```

renvoie l’aﬃchage :

```
1,  1984,  Orwel,  1949
2,  Dune,  Herbert,  1965
3,  Fondation,  Asimov,  1951
4,  Farenheit  451,  Bradbury  1953
5,  Chroniques  martiennes,  Bradbury,  1950
```

!!! abstract "Définition"
    Le mot-clé `DISTINCT` permet d’éviter l’aﬃchage de doublons.

*Exemple*

- Affichage de tous les auteurs dans la relation LIVRES

```sql
SELECT  auteur 
FROM  livres;
```

renvoie l’affichage :

```
Orwell
Herbert
Asimov
Bradbury
Bradbury
```

- Affichage de tous les auteurs dans la relation LIVRES sans les doublons

```sql
SELECT  DISTINCT  auteur 
FROM  livres;
```

renvoie l’affichage :

```
Orwell
Herbert
Asimov
Bradbury
```

!!! abstract "Définition"
    Le mot-clé `ORDER BY` permet de trier les résultats par ordre croissant.\
    Pour trier par ordre décroissant, il faut ajouter l’option `DESC`.

*Exemple*

- Affichage de tous les auteurs et titre dans la relation LIVRES par ordre croissant de l’attribut **auteur**

```sql
SELECT  titre,  auteur 
FROM  livres 
ORDER  BY  auteur;
```

renvoie l’affichage :

```
Fondation,  Asimov
Farenheit  451,  Bradbury
Chroniques  martiennes,  Bradbury
Dune,  Herbert
1984,  Orwell
```

- Affichage de tous les auteurs et titre dans la relation LIVRES par ordre décroissant de l’attribut **ann_publi**

```sql
SELECT  DISTINCT  auteur,  titre 
FROM  livres
ORDER  BY ann_publi  DESC;
```

renvoie l’affichage :

```
Dune,  Herbert
Chroniques  martiennes,  Bradbury
Fondation,  Asimov
Farenheit  451,  Bradbury
1984,  Orwell
```

!!! example "Exercice 1"
    === "Enoncé"
        On considère la relation COMMUNES dont une partie du contenu est représenté ci-dessous :

        ![](../Images/tab13.JPG)

        1. Quelle requête permet d’aﬃcher tous les noms de commune?
        2. Quelle requête permet d’aﬃcher tous les noms de commune et leur population?
        3. Quelle requête permet d’aﬃcher tous les noms de commune et leur population, classé par ordre croissant de population?
        4. Quelle requête permet d’aﬃcher tous les noms de commune et leur population, classé par ordre décroissant d’élus municipaux?
    === "Correction"
        1) `SELECT Nom_ville FROM communes ;`

        2) `SELECT Nom_ville,Population FROM communes ;`

        3) `SELECT Nom_ville,Population FROM communes  ORDER BY Population;`

        4) `SELECT Nom_ville,Population FROM communes  ORDER BY Nb_elus_municipaux DESC;`

#### Construction d'une requête avec une ou plusieurs restrictions

!!! abstract "Définition"
    Une restriction est une sélection de lignes d’une relation, sur la base d’une condition à respecter, déﬁnie à la suite du terme **WHERE** . Cette condition peut être une combinaison de comparaisons à l’aide de `AND` , de `OR` et de `NOT` (attention donc aux parenthèses dans ce cas).

!!! note "Remarque"
    Nous disposons de tous les opérateurs classiques de comparaison : $=, <>, >, >=, <, <=$.

*Exemple*

Reprenons la relation déﬁnie dans l’exemple sur la relation LIVRES.

- Affichage de tous les auteurs et titres dont le titre du livre est « 1984 »

```sql
SELECT  auteur,  titre 
FROM  livres 
WHERE  titre  =  '1984';
```

renvoie l’affichage :

```
Orwell,  1984
```

Affichage de tous les auteurs et titres dont le titre du livre n’est pas « 1984 »

```sql
SELECT  auteur,  titre 
FROM  livres 
WHERE  titre  <>  '1984';
```

renvoie l’affichage :

```
Herbert,  Dune
Asimov,  Fondation
Bradbury,  Farenheit  452
Bradbury,  Chroniques  martiennes
```

- Affichage de tous les auteurs et titres dont l’année de publication est inférieure ou égale à 1950

```sql
SELECT  auteur,  titre 
FROM  livres 
WHERE  ann_publi  <=  1950;
```

renvoie l’aﬃchage :

```
Orwell,  1984
Bradbury,  Chroniques  martiennes
```

!!! note "Remarque"
    Pour les comparaisons de chaînes de caractères, il est important de faire attention à la casse. Par déﬁnition, un "a" est donc diﬀérent d’un "A". Pour remédier à ce problème, il
    existe les fonction UPPER() et LOWER() pour transformer une chaîne en respectivement majuscule et minuscule.

```sql
SELECT  auteur,  titre 
FROM  livres 
WHERE  UPPER(titre)  =  "DUNE";
```

!!! example "Exercice 2"

    On reprend la relation COMMUNES de l’exercice 1.

    1. Quelle requête permet d’aﬃcher le nom des communes dont le nombre d’élus municipaux est strictement inférieur à 10?
    2. Quelle requête permet d’aﬃcher le nom des communes dont le nombre d’élus municipaux est strictement inférieur à 10 ET dont la population est inférieure ou égale à 100?
    3. Quelle requête permet d’aﬃcher le nombre d’élus municipaux et la population de la ville de « Cressin-Rochefort »? (NB : affranchissez vous de la casse)

!!! abstract "Définition"
    Une donnée manquante en SQL est repérée par un NULL. Il y a plusieurs raisons, bonnes ou mauvaises, pour avoir des données manquantes, et il est parfois utile de tester leur
    présence. Pour cela, nous allons utiliser le terme `IS  NULL` comme condition.

    Au contraire, si l’on veut uniquement les employés pour lesquels l’information est présente, nous devrons utiliser la négation avec `IS  NOT  NULL`.

*Exemple*

Aﬃchage de tous les auteurs et titres dont l’année de publication est connue

```sql
SELECT  auteur,  titre 
FROM  livres 
WHERE  ann_publi  IS  NOT  NULL;
```

renvoie l’aﬃchage :

```
Orwell,  1984
Herbert,  Dune
Asimov,  Fondation
Bradbury,  Farenheit  451
Bradbury,  Chroniques  martiennes
```

!!! example "Exercice 3"

    On reprend la relation COMMUNES de l’exercice 1.

    1. Quelle requête permet d’afficher le nom des communes dans lesquelles il y a eu un deuxième tour?
    2. Quelle requête permet d’afficher le nom des communes dont la population est supérieure à 5 000 habitants et dans lesquelles il y a eu un deuxième tour?

!!! abstract "Définition"
    L’opérateur `LIKE` permet de rechercher les valeurs contenant une partie seulement de la chaîne de caractères. Le caractère $\%$ représente une suite de caractères, éventuellement nulle.

*Exemple*

Affichage de tous les auteurs et titres dont le titre contient le mot « Chronique » (remarquez l’absence du s ﬁnal)

```sql
SELECT  auteur,  titre 
FROM  livres 
WHERE  titre  LIKE  "%Chronique%";
```

renvoie l’aﬃchage :

```
Bradbury,  Chroniques  martiennes
```

!!! example "Exercice 4"

    On reprend la relation COMMUNES de l’exercice 1.

    1. Quelle requête permet d’aﬃcher le nom des communes dont le nom commence par Roncherolles?
    2. Quelle requête permet d’aﬃcher le nom des communes dont le nom contient le mot « sur »?

#### Les fonctions d'agrégation


!!! abstract "Définition"
    La fonction d’agrégation `COUNT()` permet de compter le nombre d’enregistrement dans une table.

*Exemple*

Reprenons la relation déﬁnie dans le premier exemple.

- On compte tous les enregistrements présents dans la relation LIVRES.

```sql
SELECT  COUNT(*) 
FROM  livres;
```

renvoie l’affichage :


```
5
```

- On compte tous les enregistrements qui ont l’attribut ann_publi renseigné


```sql
SELECT  COUNT(ann_publi) 
FROM  livres;
```

renvoie l’affichage :

```
5
```

- On compte le nombre d’auteurs diﬀérents enregistrés


```sql
SELECT  COUNT(DISTINCT  auteur) 
FROM  livres;
```

renvoie l’affichage :


```
4
```

- On compte le nombre d’enregistrements pour lesquels l’année de publication est inférerieure ou égale à 1950


```sql
SELECT  COUNT(*) 
FROM  livres 
WHERE  ann_publi  <=  1950;
```

renvoie l’affichage :


```
2
```

!!! abstract "Définition"
    - La fonction `SUM(attribut)` permet donc de faire la somme des valeurs non nulles de l’attribut passé en paramètre.
    - La fonction `AVG(attribut)` permet de faire la moyenne des valeurs de l’attribut passé en paramètre.
    - La fonction `MEDIAN(attribut)` permet de faire la médiane des valeurs de l’attribut passé en paramètre.
    - La fonction `MIN(attribut)` permet d’afficher la valeur minimale de l’attribut passé en paramètre.
    - La fonction `MAX(attribut)` permet d’afficher la valeur maximale de l’attribut passé en paramètre.

*Exemple*

Reprenons la relation déﬁnie dans le premier exemple.

On affiche l’année de publication la plus petite :

```sql
SELECT  MIN(ann_publi) 
FROM  livres;
```

renvoie l’affichage :

```
1949
```

!!! example "Exercice 5"

    On reprend la relation COMMUNES de l’exercice 1.

    1. Quelle requête permet d’afficher le nombre total d’élus de toutes les communes?
    2. Quelle requête permet d’afficher la moyenne du nombre d’habitants dans les communes?
    3. Quelle requête permet d’afficher le nom de la ville avec la population la plus grande pour laquelle il n’y a pas de deuxième tour?

#### Les requêtes avec jointure

On a vu dans la partie consacrée au modèle relationnel, qu’une clé étrangère référence une clé primaire venant d’une autre table, suivant l’exemple ci-dessous :

![](../Images/tab6.JPG)

Pour représenter le lien entre la clé étrangère et la clé primaire, on réalise une **jointure** .

Une jointure est l’opération consistant à rapprocher selon une condition les clé primaire et clé étrangère de deux relations.

La syntaxe est : 

```sql
relation1  JOIN  relation2  
ON  relation1.attribut1  = relation2.attribut2
```

La jointure se positionne après les clauses `SELECT` et `FROM`

```sql
SELECT  attribut 
FROM  relation1 JOIN  relation2  
ON  realtion1.attribut1  =  realtion2.attribut2;
```

*Exemple*

Suivant le schéma ci-dessus,on écrirait:

```sql
livres  JOIN  auteurs  ON  livres.idAuteur  =  auteurs.id
```

!!! example "Exercice 6"

    On considère les deux relations représentés ci-dessous :

    ![](../Images/tab8.JPG)

    1. Quelle requête permet d’afficher l’auteur du livre dont le titre est « 1984 »?
    2. Quelle requête permet d’afficher tous les auteurs qui ont publié un livre avant 1960 (strictement)?
    3. Quelle requête permet d’afficher tous les titres des livres publiés par Pierre Boulle?
    4. Quelle requête permet d’afficher l’auteur et le titre de tous les livres publiés avant 1960 (strictement)?


### Les requêtes de manipulation de données

#### Insertion de données

!!! abstract "Définition"
    L’instruction de base pour l’insertion de données dans une base est constituée du mot-clé `INSERT INTO` suivi du mot-clé `VALUES` .

    - `INSERT INTO` permet de sélectionner la relation dans laquelle on insère les données.
    - `VALUES` indique les valeurs qui doivent être insérées. Elles sont indiquées entre deux parenthèses.

    La syntaxe est :

    ```sql
    INSERT  INTO  relation 
    VALUES  (attribut1  =  valeur1,  attribut2  =  valeur2) 
    WHERE  condition;
    ```

*Exemple*

```
INSERT  INTO  Livres VALUES  (4,  "Les  Furtifs",  "Damasio",  2019);
```

![](../Images/tab11.JPG)

- On insère des valeurs dans la relation Livres ;
- l’attribut id prend la valeur 4 ;
- l’attribut titre prend la valeur Les Furtifs ;
- l’attribut auteur prend la valeur Damasio ;
- l’attribut ann_publi prend la valeur 2019 .

!!! example "Exercice 7"

    Au vu du diagramme relationnel suivant, indiquez les deux requêtes pour insérer le livre d’Alain Damasio intitulé "La Horde du Contrevent", paru en 2004 et publié en langue française.

    ![](../Images/tab6.JPG)

#### Suppression de données

!!! abstract "Définition"
    L’instruction de base pour l’insertion de données dans une base est constituée du mot-clé `DELETE FROM`.

    La syntaxe est :

    ```sql
    DELETE  FROM  relation WHERE  condition
    ```

!!! example "Exercice 8"

    1. Indiquez la requête permettant de supprimer le livre "Fondation" de la relation Livres .
    2. Indiquez la requête permettant de supprimer tous les livres d’Alain Damasio de la relation Livres .
    3. Indiquez la requête permettant de supprimer tous les livres écrits avant 1945 de la relation Livres .


#### Modification de données

!!! abstract "Définition"
    L’instruction de base pour la modiﬁcation de données dans une base est constituée du mot-clé `UPDATE` , suivi du mot-clé `SET` .

    - `UPDATE` permet de sélectionner la relation dans laquelle on insère les données.
    - `SET` indique les attributs qui doivent être modiﬁés, et les valeurs correspondantes.

    La syntaxe est :

    ```sql
    UPDATE  relation
    SET  attribut  =  valeur 
    WHERE  condition;
    ```

!!! example "Exercice 9"

    1. Indiquez la requête permettant de renommer le livre "Fondation" en "Fondation - Tome 1" dans la relation Livres .
    2. Le bibliothécaire de Gonneville-la-Mallet a inscrit par erreur que le livre d’Alain Damasio "La Zone du Dehors" a été publié en 2007, or sa première édition date de 1999. Indiquez la
    requête permettant de corriger cette erreur dans la relation Livres .
