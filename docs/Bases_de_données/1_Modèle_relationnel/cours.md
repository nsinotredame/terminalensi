# Le modèle relationnel

### Un peu d'histoire

Le modèle relationnel est une manière de modéliser les relations existantes entre plusieurs informations, et de les ordonner entre elles. Cette modélisation qui repose sur des principes mathématiques mis en avant par E.F. Codd (laboratoire de recherche d’IBM) est souvent implémentée dans une base de données.

|||
|:--:|:--:|
|1970| Edgar Franck Cood pose les bases du modèle relationnel|
|1974| Création du langage SQL|
|1979| Création du premier système de gestin de base de données Oracle|
|1980| Le volume mondial de données stockées est estimé à $10^{18}$ octets|
|1990| Le volume mondial de données stockées est estimé à $10^{19}$ octets|
|1995| Première version du langage MySQL|
|2002| Le volume mondial de données stockées est estimé à $10^{20}$ octets|
|2010| Le volume mondial de données stockées est estimé à $10^{21}$ octets|
|2014| Le volume mondial de données stockés est estimé à $10^{22}$ octets|

### Les limites des outils traditionnels

#### L'exemple de la médiathèque

Prenons l’exemple d’une médiathèque. Elle souhaite recenser les ouvrages qu’elle possède. Voici le tableau qu’elle a réalisé.

![](../Images/tab1.JPG)

!!! example "Exercice 1"
    - Identiﬁez les redondances dans ce tableau, et les éléments qui semblent uniques.
    - Le livre de Ray Bradbury intitulé « Chroniques martiennes » est régulièrement indisponible car il est trop souvent emprunté. La médiathèque achète donc un deuxième exemplaire, paru chez le même éditeur. Il portera le numéro d’inventaire 236984. Complétez le
    tableau avec ce nouvel ouvrage.
    - Existe-t-il un élément unique qui va référencer totalement l’ouvrage?
    - La médiathèque s’ouvre depuis peu à la bande dessinée. Elle souhaite enregistrer l’album de bande dessinée en langue française L’incal  noir , scénarisé par Alejandro  Jodorowsky , dessiné par Moebius et édité par Les humanoïdes associés en 1982 . Quel est (sont) le(s) problème(s) rencontré(s)?

#### Notion de base de données

Une donnée est une information représentée sous une forme conventionnelle, aﬁn de pouvoir être traitée automatiquement. Une base de données (BDD) représente un ensemble ordonné de données dont l’organisation est régie par un modèle.

Les données sont généralement regroupées selon leur appartenance à un objet du monde réel.

- Le regroupement d’objets homogènes constitue une entité .
- Une entité est décrite par un ensemble d’ attributs .
- Chacun de ces attributs prend une valeur pour chaque objet.

*Exemple 1*

- Entité : un être humain
- Attributs : nom, prénom, date de naissance, lieu de naissance
- Valeurs : Young, Neil, 12 novembre 1945, Toronto

!!! example "Exercice 2"
    On souhaite représenter les données issues de la carte nationale d’identité. À partir de votre carte personnelle, donnez le nom de l’entité correspondante, les attributs qui lui sont associés
    et les valeurs qui vous sont propres.

### Le modèle relationnel

Vous pourrez visionnez cette video :

<iframe title="modele_relationnel" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/8193e14d-fae0-4fd4-a4b6-65f62b564375" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

#### Les principes de base

Un des modèles de données le plus courant est le modèle relationnel. Les principes de base de ce modèle sont les suivants :

- séparer les données dans plusieurs tables
- chaque table contient des données relatives à un même sujet
- on évite la redondance des données
- on ne stocke pas des données qui peuvent être calculées (exemple : une ligne Total)
- mettre les tables en relation par l’utilisation de clés
- clés primaires : leurs valeurs (souvent des entiers) permettent d’identiﬁer une donnée de manière unique
- clés étrangères : elles référencent une clé primaire d’une autre table

#### Un exemple : le bon de commande

La société Le Coin, située à Caen, a commandé des produits chimiques à la société BonPrixChim.

Le bon de commande est indiqué ci-dessous :

![](../Images/tab2.JPG)

À partir de ce bon de commande, on peut séparer les informations en 3 entités :

- une entité commande qui va regrouper les données de la commande;
- une entité client qui va regrouper les données du client;
- une entité produits qui va regrouper les données d’un détail.

![](../Images/tab3.JPG)

!!! example "Exercice 3"
    1. Établissez, pour chaque entité, la liste des attributs.
    2. Représentez chaque entité par une table contenant les attributs et la liste de leurs valeurs.
    3. Pensez-vous qu’il y ait des redondances? S’il y en a, modiﬁez les tables pour les supprimer.
    4. En réalité, la table Produits n’est pas pertinente. Elle ne devrait contenir que les informations associées à un produit. Quels seraient ses attributs?
    5. Construisez une quatrième table intitulé Details reprenant les attributs manquants.
    6. Comment éditera-t-on le bon de commande?

#### Définition

!!! abstract "Définition"
    Une **relation** (on parle aussi de table) est composée d’un en-tête (le libellé des attributs ) et d’un corps composé d’un ou plusieurs t-uplets (on parle aussi d’enregistrement).
    
    ![](../Images/tab4.JPG)


#### Domaine de valeurs d'un attribut

Chaque valeur possède un type et un ensemble ﬁni ou non des valeurs possibles. Cela constitue le domaine de valeurs d’un attribut.

*Exemple 2*

- L’attribut idAuteur de la relation Auteurs est un entier positif. Son type est entier . Le domaine de valeur correspond à tous les entiers de 0 à $+\infty$ .
- L’attribut titre de la relation Livres est une chaîne de caractères. Son type est chaîne de
caractères . Le domaine de valeur correspond à toutes les chaînes de caractères .

!!! example "Exercice 4"
    Déterminez le domaine de valeurs des attributs de la relation Produits .

    |reference |libelle| prix|
    |:--:|:--:|:--:|
    |1900464K  |Calcium chlorure 1 mol/L| 77|
    |31535.292 |Sodium chlorure 1 mol/L (1N) |105|
    |30024.290 |Acide chlorhydrique 1 mol/L (1N)|41|
    |30917.320 |Iode 0,05 mol/L (0,1 N) |936|

#### Notion de clé primaire

À l’intérieur d’une relation, deux t-uplets identiques ne sont pas autorisés. Il faut pouvoir identiﬁer de façon unique un t-uplet.

!!! abstract "Définition"
    Une clé primaire permet d’identiﬁer un t-uplet de manière unique.

    Il faut déterminer, parmi les attributs, lequel permet d’identiﬁer de manière unique un t-uplet.

    Cet attribut sera considéré comme la **clé primaire** de la relation.

Dans la ﬁgure précédente :

- L’attribut auteur ne peut pas jouer le rôle de clé primaire (deux ouvrages pouvant avoir le même auteur).
- De même pour les attributs titre et annPubli .
- Il reste donc l’attribut idLivre (pour identiﬁant), qui a été ajouté ici pour jouer le rôle de clé primaire.

!!! note "Remarque"
    Ici, nous avons créé artiﬁciellement une clé primaire, car aucun des autres attributs ne pouvait convenir (ce n’est pas toujours le cas).

#### Clé étrangère

Dans la relation **Auteurs** , chaque auteur est identiﬁé par l’attribut **idAuteur** (clé primaire de la relation).

Dans la relation **Livres** , on a rajouté un attribut **idAuteur** qui est la clé primaire de la relation
**Auteurs** .

!!! abstract "Définition"
    Une **clé étrangère** référence une clé primaire d’une autre table.

    ![](../Images/tab5.JPG)

L’attribut **idAuteur** est ce que l’on nomme une **clé étrangère** de la relation **Livres** , elle permet de faire le lien entre les deux relations.

!!! note "Remarque"
    Il peut y avoir plusieurs clés étrangères dans une relation.

!!! example "Exercice 5"
    1. Reprenez les relations de l’exercice 3 , en identiﬁant une clé primaire pour chacune des tables, ou en en introduisant une si elle n’existe pas.
    2. Justiﬁez que dans la relation Details , les attributs reference et numero commande ne peuvent pas constituer individuellement une clé primaire.
    3. Justiﬁez que dans la relation Details , le couple (reference, numero commande) constitue une clé primaire.
    4. Identiﬁez les clés étrangères.

#### Représentation du modèle relationnel

Le schéma d’une relation déﬁnit cette relation. Il est composé :

- du nom de la relation (on la place en tête, et en gras),
- de la liste de ses attributs avec les domaines respectifs dans lesquels ils prennent leurs valeurs (cette liste est placée entre parenthèses),
- de la clé primaire (elle est soulignée),
- des clés étrangères (on place un # entre la clé étrangère et la référence),
- des autres valeurs.

L’ensemble des relations peut être représenté soit par un schéma, soit par une notation textuelle.

*Exemple : notation textuelle*

**Livres** (<u>idLivre</u>, titre, idAuteur # Auteurs(idAuteur), annPubli)\
**Auteurs** (<u>idAuteur</u>, nom, prenom, langueEcriture)

*Exemple : schéma relationnel*

![](../Images/tab6.JPG)

!!! example "Exercice 6"
    Les relations de l'exercice 3 sont représentées ci-dessous :

    ![](../Images/tab7.JPG)

    Les clés primaires et étrangères ont été déﬁnies à l’exercice 10.

    1. Écrivez la notation textuelle du modèle relationnel.
    2. Réalisez le schéma relationnel.

### Les contraintes d'intégrité

Il est important d’assurer la cohérence et donc l’intégrité des données présentes dans une base de données. Cela consiste à s’assurer que les données stockées sont cohérentes entre elles, c’est à dire qu’elles respectent toutes les règles exigées par le concepteur de la base de données. C’est une assertion vériﬁée par les données de la base, à tout moment.

!!! abstract "Définition"
    Une contrainte d’intégrité est une règle appliquée à un attribut ou une relation et qui doit toujours être vériﬁé.

    Les contraintes d’intégrité sont vériﬁées par le système de gestion des bases de données (SGBD).

    Si l’une des règle n’est pas respectée, le SGBD signalera cette erreur et n’autorisera pas l’écriture de cette nouvelle donnée.

#### La contrainte de domaine

!!! abstract "Définition"
    Chaque attribut doit prendre une valeur dans son domaine de valeurs.

*Exemple*

- La note obtenue dans une matière doit être comprise entre 0 et 20;
- la quantité commandée est obligatoire ET doit être strictement supérieure à 0.

#### La contrainte de relation

!!! abstract "Définition"
    Chaque relation dans le modèle relationnel est identiﬁée par une clé primaire qui doit être **unique** et **non nulle** . Donc, chaque t-uplet est également identiﬁé par une clé primaire.

#### La contrainte de référence

!!! abstract "Définition"
    Une clé étrangère dans une relation doit être une clé primaire dans une autre. De plus, le domaine de valeurs de ces deux clés doit être identique. Enﬁn, la valeur d’une clé étrangère doit exister dans la clé primaire qui y fait référence.


*Exemple*

Dans la ﬁgure ci-dessous, la valeur de la clé étrangère idAuteur ne peut pas être supérieure à 8.

![](../Images/tab8.JPG)