### Exercices

!!! example "Exercice 7"
    Dans l’exemple de la Médiathèque, on souhaite créer 3 relations :

    -  Auteur , contenant toutes les informations sur un auteur
    -  Livre , contenant toutes les informations sur un livre
    -  Ouvrage , contenant toutes les informations sur un ouvrage disponible à l’emprunt.

    1. Pour chacune des relations, indiquez les attributs avec leur type et leur domaine de valeurs.
    2. Pour chacune des relations, indiquez la clé primaire et éventuellement la ou les clé(s) étrangère(s).
    3. Représentez le schéma relationnel correspondant, ainsi que la notation textuelle qui lui est associée.

!!! example "Exercice 8"
    Un laboratoire souhaite gérer les médicaments qu’il conçoit :

    - Un médicament est décrit par un nom, qui permet de l’identiﬁer. En effet il n’existe pas deux médicaments avec le même nom.
    - Un médicament comporte une description courte en français, ainsi qu’une description longue en latin.
    - On gère aussi le conditionnement du médicament, c’est à dire le nombre de pilules par boîte (qui est un nombre entier).
    - À chaque médicament on associe une liste de contre-indications, généralement plusieurs, parfois aucune.
    - Une contre-indication comporte un code unique qui l’identiﬁe, ainsi qu’une description.
    - Une contre-indication est toujours associée à un et un seul médicament.

    Voici deux exemples de données :

    - Le Chourix a pour description courte « Médicament contre la chute des choux » et pour description longue « Vivamus fermentum semper porta. Nunc diam velit, adipiscing ut tristique vitae, sagittis vel odio. Maecenas convallis ullamcorper ultricies. Curabitur ornare. ». Il est conditionné en boîte de 13.\
    Ses contre-indications sont :

    - CI1 : Ne jamais prendre après minuit.
    - CI2 : Ne jamais mettre en contact avec de l’eau.

    - Le Tropas a pour description courte « Médicament contre les dysfonctionnements intellectuels » et pour description longue « Suspendisse lectus leo, consectetur in tempor sit amet,
    placerat quis neque. Etiam luctus porttitor lorem, sed suscipit est rutrum non. ». Il est conditionné en boîte de 42.\
    Ses contre-indications sont :

    - CI3 : Garder à l’abri de la lumière du soleil.
    
    1. Donnez la représentation sous forme de tables des relations Medicament et ContreIndication .
    2. Écrivez le schéma relationnel permettant de représenter une base de données pour ce laboratoire.

