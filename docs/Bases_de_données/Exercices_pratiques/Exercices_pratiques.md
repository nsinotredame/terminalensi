# Exercices pratiques

## Base de données sur les prix Nobel

### Exploration de la base

!!! example "Exercice 1"

    1) Pour connaître l'ensemble des tables, il suffit d'écrire 

    ```sql
    SELECT * 
    FROM sqlite_master 
    WHERE TYPE ="table";
    ``` 

    et de valider.

    2) A l'aide de la cellule ci-dessous, répondre aux questions :

    {!{ sqlide titre="Base Prix Nobel" base="Bases_de_données/Documents/nobel.sqlite" }!}

    a) Combien de relation possède la base de données?

    b) Combien d'attributs possède la relation **nobel**?

    c) Quel est le type de l'attribut **annee**?

### Les requêtes d'interrogation

**Chaque requête devra se terminer par un `;` avant d'être exécutée.**

!!! example "Exercice 2"

    {!{ sqlide titre="Base Prix Nobel" base="Bases_de_données/Documents/nobel.sqlite" }!}

    1. Comment afficher le nom de tous les lauréats?
    2. Comment afficher le nom de toutes les disciplines **en évitant les doublons**?
    3. Quelle est la discipline de **Wilhelm Conrad Röntgen**?
    4. Dans quelle discipline **Paul Krugman** est-il devenu Prix Nobel?
    5. En quelle année **Albert Fert** a-t-il eu le prix Nobel?
    6. Quelle est l’année de distinction de **Pierre Curie**?
    7. Quelle est l’année de distinction et la matière de **Bertha von Suttner**?
    8. Quels sont les lauréats distingués au XXI e siècle?
    9. Quels sont les lauréats du prix Nobel de la Paix durant la deuxième guerre mondiale?
    10. Quels sont les lauréats distingués en Médecine en 1901 et 2001?
    11. Quels sont les lauréats des prix nobel de Physique et de Médecine en 2008?

### Les requêtes d'agrégation

!!! example "Exercice 3"

    {!{ sqlide titre="Base Prix Nobel" base="Bases_de_données/Documents/nobel.sqlite" }!}

    1. Combien d’enregistrements au total comporte la relation?
    2. Combien de personnes ont reçu le prix Nobel de la paix?
    3. Combien de personnes ont reçu le prix Nobel de litérature?
    4. Combien de personnes ont reçu le prix Nobel de mathématiques?
    5. Combien de personnes ont reçu un prix Nobel en 1901?
    6. Combien de personnes ont reçu un prix Nobel de chimie en 1939?
    7. En quelle année a été décerné le premier prix Nobel d’économie?
    8. Combien de prix Nobel a reçu **Marie Curie**?
    9. Quels sont les prix lauréats, leur discipline et l’année de disctinction de tous les prix Nobel contenant cohen dans leur nom (on ne fera pas de distinction de casse)?
    10. Combien y’a t’il eu de lauréats en Physique et en Chimie?
    11. Combien y’a t’il eu de lauréats de Médecine et de littérature en 2000?
    12. Nombre de lauréats diﬀérents parmi les prix nobels de la paix?

### Les requêtes d'insertion

!!! example "Exercice 4"

    {!{ sqlide titre="Base Prix Nobel" base="Bases_de_données/Documents/nobel.sqlite" }!}

    1. En 2019, Esther Duﬂo a reçu le prix Nobel d’économie. Écrivez la requête permettant d’insérer cet enregistrement.
    2. Quelle requête permet de modiﬁer l’enregistrement précédent pour accoler le nom d’époux (Banerjee) après celui de Duﬂo?
    3. De nombreuses pétitions circulent pour retirer le prix Nobel à Aung San Suu Kyi. Quelle requête permettrait cela?


## Base de données collectivités

### Exploration de la base

!!! example "Exercice 5"

    Dans la cellule ci-dessous, on a chargé la base de donnée `Collectivites.db`.

    {!{ sqlide titre="Base Collectivites" base="Bases_de_données/Documents/Collectivites.db" }!}

    1. Combien de relation possède la base de données?
    2. Réalisez un schéma relationnel de cette base de données, sous la forme graphique, en précisant pour chaque attribut son type et s'il doit impérativement être rempli.

### Collecte des informations

!!! example "Exercice 6"

    La base **Collectivites** est vide. Il faut la remplir. Pour cela, en vous aidant d'internet, complétez les tableaux suivants :

    |idCommune|Commune|Code Postal|Département|Nombre d'habitants|
    |--:|--:|--:|--:|--:|
    ||Rouen||||
    ||Dieppe||||
    ||Enverneu||||
    ||Le Neubourg|27110|Eure||
    ||Igoville||||


    |idDepartement|Département|Code d'immatriculation|
    |--:|--:|--:|
    ||Seine-Maritime||
    ||Eure||
    ||Calvados||


### Insertion des enregistrements

!!! example "Exercice 7"

    {!{ sqlide titre="Base Collectivites" base="Bases_de_données/Documents/Collectivites.db" }!}

    1. Insérez le département de Seine-Maritime.
    2. Insérez la commune de Rouen.
    3. Faîtes de même, en une seule requête, avec les communes de Dieppe et d’Envermeu.
    4. Insérez la commune d’Igoville.
    5. Insérez la commune du Neubourg.

!!! example "Exercice 8"

    {!{ sqlide titre="Base Collectivites" base="Bases_de_données/Documents/Collectivites.db" }!}

    En recherchant éventuellement les informations manquantes, indiquez le code SQL permettant de répondre aux consignes suivantes :

    1. Trouville, Mézidon-Canon et Crèvecoeur-en-Auge sont des villes du Calvados.
    2. Le vrai nom de Trouville est en fait Trouville-Sur-Mer.
    3. Médizon-Canon et Crèvecoeur-en-Auge n’existent plus. Elles ont fusionné pour donner une nouvelle commune : Mézidon-Vallée-d’Auge.

### Vérification des enregistrements

!!! example "Exercice 9"

    {!{ sqlide titre="Base Collectivites" base="Bases_de_données/Documents/Collectivites.db" }!}

    Indiquez le code SQL permettant de répondre aux consignes suivantes :

    1. Combien il y a-t-il de départements différents enregistrés dans la base? (réponse : 3)
    2. Combien il y a-t-il de communes différentes enregistrées dans la base? (réponse : 7)
    3. Combien il y a-t-il de communes dans l’Eure enregistrées dans la base? (réponse : 2)
    4. Combien il y a-t-il de communes en Seine-Maritime enregistrées dans la base? (réponse : 3)
    5. Combien il y a-t-il de communes dans le Calvados enregistrées dans la base? (réponse : 2)

## Base consacrée au cinéma

### Exploration de la base de données

!!! example "Exercice 10"

    Dans la cellule ci-dessous, on a chargé la base de donnée `Cinema.sqlite`.

    {!{ sqlide titre="Base Cinema" base="Bases_de_données/Documents/Cinema.sqlite" }!}

    1. Combien de relations possède la base de données?
    2. Combien d’attributs possède la relation Artiste?
    3. Quelle est sa clé primaire?
    4. Combien d’attributs possède la relation Film?
    5. Quelle est sa clé primaire?
    6. Quelle est sa clé étrangère?
    7. Quelle est la référence de la clé étangère.
    8. En procédant de la même façon pour toutes les tables, représentez le schéma relationnel sous forme graphique, en faisant bien ﬁgurer les relations entre les clé primaires et clés étrangères.

### Requêtes simples

!!! example "Exercice 11"

    {!{ sqlide titre="Base Cinema" base="Bases_de_données/Documents/Cinema.sqlite" }!}

    Indiquez le code SQL permettant d’afficher :

    1. les titres des ﬁlms triés par ordre alphabétique.
    2. Les prénoms, noms et année de naissance des artites nés avant 1950.
    3. Les cinémas qui sont situés dans le 12ème arrondissement.
    4. Les artistes dont le nom commence par la lettre H (on utilisera la commande LIKE).
    5. Les acteurs dont on ignore la date de naissance (cela signiﬁe que la valeur n’existe pas).
    6. Le nombre de fois où Bruce Willis a joué le rôle de McLane (on utilisera la commande UPPER() pour s’affranchir de la casse).

### Requêtes avec jointure

!!! example "Exercice 12"

    On rapelle qu’une jointure consiste à rapprocher les clé primaire et étrangère de deux relations.

    La syntaxe est la suivante :

    ```sql
    SELECT  attribut 
    FROM  relation1 JOIN  relation2  
    ON  relation1.cle1  =  relation2.cle2;
    ```

    {!{ sqlide titre="Base Cinema" base="Bases_de_données/Documents/Cinema.sqlite" }!}

    Indiquez le code SQL permettant de répondre aux questions suivantes :

    1. Quel est le nom et le prénom de l’acteur qui a joué Tarzan (pensez à la commande UPPER()).
    2. Quelle est l’année de naissance du réalisateur de Reservoir Dogs?
    3. Quels sont les titres des ﬁlms dans lesquels a joué Woody Allen. Donnez aussi le rôle joué.
    4. Quels ﬁlms peut-on voir au cinéma Rex? (Attention aux doublons)
    5. Quels ﬁlms peut-on voir à 15h dans un cinéma parisien?
    6. Quels sont les cinémas (nom, adresse et arrondissement) qui diﬀusent des ﬁlms le matin.
    7. Quels sont les cinémas (nom, adresse et arrondissement) qui diﬀusent des ﬁlms le matin.\
    Indiquez aussi le titre du ﬁlm.
    8.Quels ﬁlms peut-on voir dans un cinéma du 12 e arrondissement? On donnera le titre du ﬁlm, le cinéma dans lequel il est joué et son adresse.
    9. Quels sont les nom et prénom des acteurs qui ont joué dans le ﬁlm Vertigo.
    10. Quel réalisateur a tourné dans ses propres ﬁlms? Donnez le nom, le rôle et le titre des ﬁlms.
    11. Où peut-on voir le ﬁlm Pulp Fiction? On donnera le nom, l’adresse du cinéma et numéro de la séance.
    12. Où peut-on voir un ﬁlm avec Clint Eastwood? On donnera le titre du ﬁlm, le nom et l’adresse du cinéma, ainsi que l’heure de début du ﬁlm.
    13. Quels sont les cinémas (nom, adresse et arrondissement) ayant une salle de plus de 150 places et passant un ﬁlm avec Bruce Willis?

### Modification de la base cinéma

!!! example "Exercice 13"

    {!{ sqlide titre="Base Cinema" base="Bases_de_données/Documents/Cinema.sqlite" }!}

    1. On souhaite insérer un nouvel artiste. Il s’agit de Ridley Scott, né en 1937. Indiquez la requête SQL permettant de réaliser cette opération.
    2. Ridely Scott est le réalisateur du ﬁlm Blade Runner, sorti en 1982. Indiquez la requête SQL permettant de saisir ce nouvel enregistrement.
    3. Les acteurs principaux sont Harisson Ford (né en 1942) dans le rôle de Rick Deckard et Rutger Hauer (né en 1944) dans le rôle de Roy Batty. Indiquez la requête SQL permettant de saisir ces nouveaux enregistrements.
    4. Mark Hamill (né en 1951), Harisson Ford (né en 1942) et Carrie Fisher (née en 1956) sont les acteurs principaux du ﬁlm L’empire contre attaque réalisé en 1980 par Irvin Kershner (né en 1923). Indiquez les requêtes SQL permettant de saisir ces enregistrements.
    Indiquez l’ordre dans lequel il faut remplir les relations.
    5. Le réalisateur moldave Mevatlave Kraspeck est décédé brutalement hier à l’âge de 103 ans. Le Rex décide de lui rendre hommage en diﬀusant le ﬁlm Gant, Savon, Serviette et Ponge tourné en 1957 avec la star du cinéma muet Agathe Zeblouse (née en 1932) dans la
    salle 1 de 13h à 16h, le ﬁlm Shower on the Beach tourné en 1963 avec Camille Honnête (née en 1940) dans la salle 1 de 16h à 18h et Bains Moussants avec Jacques Célère (né en 1945 et décédé trop vite en 1969) tourné en 1967 dans la salle 2 de 15h à 18h. Indiquez
    les requêtes SQL permettant de modiﬁer les séances du cinéma Rex.



## SQL Murder Mystery

!!! example "SQL Murder Mystery"
    Un meurtre  a été commis.  Comme vous  ne prenez jamais  de notes, vous  ne vous
    souvenez plus du nom du meurtrier. Vous  vous souvenez juste que le meurtre a eu
    lieu le 15 janvier 2018 dans la ville de SQL City.


    ![e g robinson](../Images/egr.webp)

    Vous disposez ensuite de la base de données schématisée ici :

    ![bdd de sql murder mystery](../Images/sqlmm.png)

    A vous de jouer :


    {!{ sqlide titre="Entrez ici vos requêtes pour mener l'enquête:" base="Bases_de_données/Documents/sql-murder-mystery.db" espace="bddmm"}!}


    Vous rentrerez ensuite votre solution ici :



    {!{ sqlide titre="Votre solution:" sql="Bases_de_données/Documents/sqlmm-sol.sql" espace="bddmm"}!}

## SQLZOO

!!! example "SQLZOO : site pour s'entrainer à la manipulation de bases de données sql"

    Base de données World : (Lien vers sqlzoo)[https://sqlzoo.net/wiki/SUM_and_COUNT]
    
    Base de données  Euro2012 : (Lien vers sqlzoo)[https://sqlzoo.net/wiki/The_JOIN_operation]
