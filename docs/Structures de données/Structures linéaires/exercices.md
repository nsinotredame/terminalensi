# Structures linéaires : listes piles files

!!! example "Exercice 1"
    Quelle structure de données choisir pour chacune des tâches suivantes :

    - Représenter un répertoire téléphonique.
    - Envoyer des fichiers à un serveur d'impression. (un spooler en anglais).
    - Gérer dans un logiciel les commandes Undo et Redo (Annuler et refaire).

!!! example "Exercice 2"
    
    On donne la séquence d'instructions suivante :

    ```
        L1 = CREER_LISTE_VIDE()
        L2 = CREER_LISTE_VIDE()
        INSERER(L1,10,1)
        INSERER(L1,20,2)
        INSERER(L1,30,3)
        INSERER(L1,40,4)
        INSERER(L2,LIRE(L1,1),1)
        INSERER(L2,LIRE(L1,2),1)
        INSERER(L2,LIRE(L1,3),1)
        INSERER(L2,LIRE(L1,4),1)
    ```
            

    - Illustrer le résultat de chaque étape de la séquence.
    - Quelle est l'opération effectuée ?

!!! example "Exercice 3"
    On donne la séquence d'instructions suivante :

    ```
    F = CREER_FILE_VIDE()
    ENFILER(F,5)
    ENFILER(F,8)
    ENFILER(F,9)
    R = DEFILER(F)
    ENFILER(F,7)
    R = DEFILER(F)
    ```

    Illustrer le résultat de chaque étape de la séquence. 

!!! example "Exercice 4"
    On dispose d'une file F1 qui contient les éléments suivants classés par ordre alphabétique :

    ```
    F1=('A','B','C','D','E')
    ```

    Quel est l'élément issu du défilage de F1 ?
    Proposer un algorithme écrit en pseudo-code qui utilise deux piles P1 et P2 et qui permet la saisie d'affilée des 5 éléments (sans sortie intermédiaire) et la sortie des éléments comme s'ils sortaient d'une file.

!!! example "Exercice 5"
    === "Enoncé"
        
        - Implémenter une file et une pile à l'aide d'une classe File et d'une classe Pile
        - Implémenter une classe FilePile, qui représente une file mais utiliser deux piles pour l'implémenter au lieu d'une liste
<<<<<<< HEAD
    
=======

>>>>>>> origin/main
    === "Corrigé première question"

        ```python
        class Pile:
            def __init__(self):
                self.valeurs = []

            def empiler(self,element):
                self.valeurs += [element]
            
            def depiler(self):
                valeur = self.valeurs[-1]
                self.valeurs = self.valeurs[:-1]
                return valeur

            def est_vide(self):
                return self.valeurs == []
            

            
        class File:
            def __init__(self):
                self.valeurs = []

            def enfiler(self,element):
                self.valeurs += [element]
            
            def defiler(self):
                valeur = self.valeurs[0]
                self.valeurs = self.valeurs[1:]
                return valeur
            
            def est_vide(self):
                return self.valeurs == []
<<<<<<< HEAD
        ```
=======
        ```
>>>>>>> origin/main
