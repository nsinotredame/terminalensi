from mon_viz import viz

class Maillon():

    def __init__(self, val) :
        self._val = val
        self._suiv = None

    def get_val(self):
        return self._val 

    def get_suiv(self):
        return self._suiv

    def set_suiv(self, m):
        if m is None:
            self._suiv = m


class ListeC():

    def __init__(self):
        self._tete = None

    def est_vide(self):
        return self._tete is None

    def get_val_tete(self):
        return self._tete.get_val()

    def get_suiv_tete(self):
        return self._tete.get_suiv()

    def queue(self):
        qt = ListeC()
        if not self._tete is None:
            qt._tete = self._tete.get_suiv() 
        return qt

    def set_tete(self, val):
        if self._tete is None:
            self._tete = Maillon(val)
            return self
        else:
            t = Maillon(val)
            t.set_suiv(self._tete)
            self._tete = t
            return self
            