# Arbres
## Structures de données hiérarchiques : les arbres

!!! abstract "Définition"
    Un arbre est une structure hiérarchique permettant de représenter de manière symboloique des informations structurées, par exemple : 

    - un dossier, contenant des dossiers et des fichiers
    - un arbre généalogique des descendants ou des ascendants (arbre binaire)
    - une tâche complexe décomposée en tâches élémentaires et en tâches complexes...

    Précisément, un **arbre** est un **graphe sans cycle** où des **noeuds** sont reliés par des **arêtes**.

    On distingue trois sortes de noeuds:

    - Les **noeuds internes**, qui ont des **enfants**
    - Les **feuilles** qui n'ont pas d'enfants
    - La **racine** de l'arbre qui est l'unique noeud ne possédant pas de **parent**

    En informatique, les arbres poussent vers le bas!

    Un arbre est caractérisé par :

    - son **arité** : le nombre maximal d'enfant(s) qu'un noeud peut avoir
    - sa **taille** : le nombre de noeud(s) qui le compose
    - sa **hauteur** : la profondeur à laquelle il faut descendre pour trouver la feuille la plus éloignée de la racine, donc la profondeur maximale.

    La **profondeur** d'un noeud étant la distance, c'est à dire le nombre d'arêtes, de la racine au noeud.

!!! example "Exercice 1"

    ```mermaid
    flowchart TD
    A((A))
    B((B))
    C((C))
    D((D))
    E((E))
    F((F))
    G((G))
    H((H))
    I((I))
    J((J))
    K((K))
    A-->B
    A-->C
    A-->D
    B-->E
    B-->F
    D-->G
    D-->H
    D-->I
    I-->J
    I-->K
    ```

    Pour l'arbre ci dessus, préciser : 
    
    - La racine
    - Les feuilles et noeuds internes
    - Son arité
    - Sa taille
    - La profondeur de H
    - La hauteur de l'arbre
    - Les adelphes de B (noeuds ayant le même parent)

!!! example "Exercice 2"
    Un arbre permet de représenter une expression algébrique :

    ```mermaid
    flowchart TD
    A(("*"))
    B(("+"))
    C(("-"))
    E(("1"))
    F(("2"))
    G(("5"))
    H(("/"))
    I(("8"))
    J(("2"))
    A-->B
    A-->C
    B-->E
    B-->F
    C-->G
    C-->H
    H-->I
    H-->J
    ```

    1. Préciser les feuilles, les noeuds internes.

    2. Quelle est la taille de cet arbre? Sa hauteur?

    3. Quelle est la valeur de cette expression?

!!! example "Exercice 3"

    |Noeud|Etiquette|Noeud du SAG|Noeud du SAD|
    |-----|---------|------------|------------|
    |1|*|2|3|
    |2|+|4|5|
    |3|-|6|7|
    |4|3|||
    |5|/|8|9|
    |6|8|||
    |7|*|10|11|
    |8|4|||
    |9|2|||
    |10|2|||
    |11|3|||

    1. Représenter l'arbre correspondant.

    2. Quelle est la hauteur de cet arbre? Quelle est la valeur de cette expression algébrique?

## Les arbres binaires

!!! abstract "Définition"
    Un **arbre binaire** est un arbre où chaque noeud possède **au plus 2 enfants**. A chaque noeud d'un arbre binaire, on associe :

    - une **clé** (valeur associée au noeud)
    - un **sous-arbre gauche** (SAG)
    - un **sous-arbre droit** (SAD)

    Un arbre (ou sous-arbre) peut être **vide**. En particulier au niveau des feuilles les SAG et SAD sont vides!

    ```mermaid
    flowchart TD

        subgraph SAG
            B -->C
            B -->D
            C -->E
        end
        subgraph SAD
            F -->H
            F -->G
            G -->I
        end
    
        A --> B
        A --> F

    ```

!!! info "Cas particuliers d'arbres binaires"

    - Arbre **dégénéré** : arbre dont les noeuds ne possèdent que 1 ou 0 enfant

    - Arbre **localement complet** : arbre dont chacun des noeuds possède soit 2 enfants soit aucun

    - Arbre **complet** : arbre binaire qui est localement complet et dont toutes les feuilles sont au niveau hiérarchique le plus bas

!!! example "Exercice 4"
    Dans cet exercice, on note `h` la hauteur d'un arbre binaire (h est un entier positif) et `N` sa taille. Exprimer `N` en fonction de `h` lorsque l'arbre est degénéré; puis lorsque l'arbre est complet. En déduire que : `log2(N+1) -1 <= h <= N-1`

!!! example "Exercice 5"
    1. Dessiner tous les arbres binaires ayant 3 noeuds.

    2. Combien y-a-t-il d'arbres binaires avec 4 noeuds?

!!! abstract "Mesures sur les arbres binaires"

    - La **taille** d'un arbre A est définie par

        - `T(A) = 0` si A est un arbre vide
        - `T(A) = 1 + T(SAG(A)) + T(SAD(A))`  sinon

    - La **profondeur** d'un noeud x dans un arbre A est définie par :

        - `P(x) = 0` si x est la racine de A
        - `P(x) = 1 + P(y)`  si y est le père de x

    - La **hauteur** d'un arbre A est définie par:
        - `H(A) = Max(P(x))` où x est un noeud de A

!!! example "Exercice 6"
    En reprenant le premier arbre en exemple du cours, calculer en détaillant les calculs `P(E)`

!!! abstract "Arbre binaire : type abstrait"
    Un type abstrait de données est un ensemble de données doté d'un ensemble d'opérations dont on précise seulement les spécifications. On fait donc à la fois abstraction de la manière dont les données sont représentée et de la manière dont les opérations sont progammées.

    Le concept de type abstrait n'est pas lié au langage de programmation, il peut être mis en oeuvre via des mécanismes de programmation, dont en particulier la POO ou la programmation avec des fonction. Parmi les opérations sur un type de données abstrait, on distingue:

    - les **constructeurs**, qui permettent de créer des données
    - les **sélecteurs**, qui permettent d'accéder à l'information contenue dans une donnée
    - les **opérateurs**, qui permettent d'opérer entre données du type ou avec d'autres types de données
    - les **prédicats**, qui permettent de tester une propriété.

    L'ensemble des fonctionnalités disponibles pour un type de données en constitue **l'interface**:
    la partie visible pour qui veut utiliser ce type de données.

    L'**implémentation** consiste à concrétiser effectivement un type de données en définissant la représentation des données et en écrivant les programmes des opérations.

## Interface du type abstrait "arbre binaire"

!!! info "Interface du type abstrait arbre binaire "

    - Constructeurs :

        - `créer_arbre_vide()` pour créer un arbre vide
        - `arbre(r, g, d)` création d'un arbre de racine r, de SAG g et de SAD d

    - Selecteurs :

        - `gauche(A)` renvoie le SAG de A
        - `droit(A)` renvoie le SAD de A
        - `racine(A)` renvoie la racine/étiquette de A

    - Prédicats :

        - `estVide(A)` renvoie `True` si A est vide et `False` sinon
        - `estFeuille(A)` renvoie `True` si A est une feuille et `False` sinon

Exemple d'utilisation de l'interface "Arbre binaire"

```mermaid
flowchart TD
A((A))
B((B))
C((C))
D((D))
A-->B
A-->D
B-->C
```

```python
D = arbre("D", creer_arbre_vide(), creer_arbre_vide())
C = arbre("C", creer_arbre_vide(), creer_arbre_vide())
B = arbre("B", creer_arbre_vide(), C)
A = arbre("A", B, D)
```

!!! example "Exercice 7" 
    En utilisant l'interface "arbre binaire", construire l'arbre dans la définition d'arbre binaire du cours

## Parcours d'arbres

- Un parcours **infixe**, on visite chaque noeud **entre** les noeuds de son SAG et les noeuds de son SAD

    ```
    fonction parcours_infixe(A){
        # données : A est un arbre binaire
        # retour : rien, affiche les noeuds de A pour le parcours infixe

        si non(estVide(A)){
            parcours_infixe(gauche(A))
            afficher(racine(A))
            parcours_infixe(droit(A))
        }
    }
    ```

- Un parcours **préfixe**, on visite chaque noeud **avant** les noeuds de son SAG et les noeuds de son SAD

    ```
    fonction parcours_infixe(A){
        # données : A est un arbre binaire
        # retour : rien, affiche les noeuds de A pour le parcours infixe

        si non(estVide(A)){
            afficher(racine(A))
            parcours_infixe(gauche(A))
            parcours_infixe(droit(A))
        }
    }
    ```

- Un parcours **postfixe**, on visite chaque noeud **après** les noeuds de son SAG et les noeuds de son SAD

    ```
    fonction parcours_infixe(A){
        # données : A est un arbre binaire
        # retour : rien, affiche les noeuds de A pour le parcours infixe

        si non(estVide(A)){
            parcours_infixe(gauche(A))
            parcours_infixe(droit(A))
            afficher(racine(A))
        }
    }
    ```

!!! example "Exercice 8" 
    Donner les 3 parcours pour : 

    ```mermaid
    flowchart TD
    A((A))
    B((B))
    C((C))
    A-->B
    A-->C
    ```

    Puis pour : 

    ```mermaid
    flowchart TD
    A((A))
    B((B))
    D((D))
    E((E))
    F((F))
    H((H))
    I((I))
    J((J))
    K((K))
    A-->B
    A-->D
    B-->E
    B-->F
    D-->H
    D-->I
    I-->J
    I-->K
    ```

## Application en python
!!! example "Implémentation"
    === "Enoncé"
        
        1\. A vous d'utiliser la programmation orientée objet pour créer la classe ArbreBinaire, l'implémentation du type abstrait "arbre binaire" en python.

        2\. Une fois le type abstrait "arbre binaire" implémenté, ajouter les méthodes de parcours infixe, préfixe et suffixe


    === "Implémentation possible"

        ```python
        class ArbreBinaire:
            def __init__(self,r = None,g = None,d = None):
                self.r = r
                self.g = g
                self.d = d
            
            def gauche(self):
                return self.g
            
            def droit(self):
                return self.d
            
            def racine(self):
                return self.racine

            def estVide(self):
                return self.r == None and self.g == None and self.d == None
            
            def estFeuille(self):
                return self.g == None and self.d == None
        ```
        Méthode estFeuille un peu plus élégante:
        
        ```python
            def estFeuille(self):
                return self.g.estVide() and self.d.estVide()
        ```
        Implémentation de l'arbre ABCD
        ```python
        D = ArbreBinaire("D", ArbreBinaire(), ArbreBinaire())
        C = ArbreBinaire("C", ArbreBinaire(), ArbreBinaire())
        B = ArbreBinaire("B", ArbreBinaire(), C)
        A = ArbreBinaire("A", B, D)
        ```

    === "Méthodes de parcours"

        Méthodes de parcours:

        ```python
            def infixe(self):
                if not(self.estVide()):
                    self.g.infixe()
                    print(self.r)
                    self.d.infixe()
        ```


        Autres méthodes
        ```python
        def hauteur(self):
            if self.estVide():
                return -1
            else:
                return 1 + max(self.gauche().hauteur(), self.droit().hauteur())

        def taille(self):
            if self.estVide():
                return 0
            else:
                return 1 + self.gauche().hauteur() + self.droit().hauteur()
        ```