class ArbreBinaire:
    def __init__(self,r = None,g = None,d = None):
        self.r = r
        self.g = g
        self.d = d
    
    def gauche(self):
        return self.g
    
    def droit(self):
        return self.d
    
    def racine(self):
        return self.racine

    def estVide(self):
        return self.r == None and self.g == None and self.d == None
    
    def estFeuille(self):
        return self.g.estVide() and self.d.estVide()

    def infixe(self):
        if not(self.estVide()):
            self.g.infixe()
            print(self.r)
            self.d.infixe()
    
    def prefixe(self):
        if self.estFeuille():
            print(self.r)
        else:
            print(self.r)
            self.g.infixe()
            self.d.infixe()

    def postfixe(self):
        if self.estFeuille():
            print(self.r)
        else:
            self.g.infixe()
            self.d.infixe()
            print(self.r)

    def hauteur(self):
        if self.estVide():
            return -1
        else:
            return 1 + max(self.gauche().hauteur(), self.droit().hauteur())

    def taille(self):
        if self.estVide():
            return 0
        else:
            return 1 + self.gauche().hauteur() + self.droit().hauteur()
    

D = ArbreBinaire("D", ArbreBinaire(), ArbreBinaire())
C = ArbreBinaire("C", ArbreBinaire(), ArbreBinaire())
B = ArbreBinaire("B", ArbreBinaire(), C)
A = ArbreBinaire("A", B, D)

A.infixe()