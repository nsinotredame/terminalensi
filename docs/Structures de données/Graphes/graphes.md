# Graphes

!!! abstract "Structure de données : graphe"
    Un graphe est une structure de données composée de deux types d'éléments : les sommets (ou noeuds) et les arêtes (qui lient les sommets entre eux)

    On peut utiliser les graphes pour représenter plusieurs informations, mais généralement c'est utilisé pour représenter des relations entre des objets (par exemple un réseau social)

    Un graphe est une manière abstraite de représenter l'information, ainsi, la manière dont on choisis de la représenter graphiquement n'a pas d'importance

    Voici deux graphes toalement identiques:
    ```mermaid
    flowchart TD
    A---B
    A---C
    A---D
    B---E
    B---C
    ```

    ```mermaid
    flowchart LR
    A---B
    A---C
    A---D
    B---E
    B---C
    ```

## Graphes orientés, non orientés

Il existe deux types de graphes, ceux qui sont orientés, et les autres.

Chaque type de graphe a son vocabulaire propre. Ainsi, si pour un graphe non orienté on parle de **sommets** et **d'arêtes**, pour un graphe non orienté on parle toujours de **sommets**, mais à la place des **arêtes** on a des arcs

!!! abstract "Graphes non orientés"

    Dans un graphe non orienté, les relations entre les sommets sont réciproques, si un sommet A est relié à un sommet B, alors le sommet B est relié au sommet A, on dit que la relation est symétrique.


    ```mermaid
    flowchart LR
    A---B
    E---C
    A---D
    B---E
    B---C
    D---C
    ```

!!! abstract "Graphes orientés"
    
    Dans un graphe orienté, les relations ne sont pas forcément récipqorues, si un sommet A est relié à un sommet B, le sommet B n'est pas forcément relié au sommet A.

    ```mermaid
    flowchart LR
    A-->B
    E-->C
    A-->D
    B-->E
    B-->C
    D-->C
    ```

## Graphes simples

!!! abstract "Graphes simples et multi-graphes"
    On dit d'un graphe qu'il est simple si il ne peut y avoir qu'une arête entre deux sommets, dans un graphe non orienté, ou deux arcs, dans un graphe orienté.

    Dans le cas contraire on parle de **multi-graphes**:

    ```mermaid
    flowchart LR
    A-->B
    A-->B
    E-->C
    D-->A
    D-->A
    B-->E
    B-->C
    D-->C
    ```

## Propriétés des graphes:

!!! abstract "Propriétés des graphes"
    Il existe différentes propriétés permettant d’exprimer la forme, la taille ou la 
    complexité d’un graphe :

    - **l’ordre** d’un graphe est le nombre de ses sommets S

    - la **taille** d’un graphe est le nombre de ses arêtes A

    - le **degré** d’un sommet est le nombre d’arêtes qui relient ce sommet à d’autres sommets
    
    - la **densité** D d'un graphe simple est une indication générale de sa connectivité et indique s'il a "beaucoup" ou "peu" d'arêtes. Différentes métriques existent, parmi lesquelles le rapport entre le nombre d'arêtes existantes et leur nombre possible:
     
        - Pour un graphe orienté D = $\frac {A}{S*(S-1)}$

        - Pour un graphe non-orienté D = $\frac {A*2}{S*(S-1)}$

    - le **degré entrant** d’un sommet dans un graphe orienté est le nombre d’arcs entrants, c’est-à-dire qui ont ce sommet comme destination. De même le **degré sortant** est le nombre d’arcs sortants, c’est-à-dire qui ont ce sommet comme origine. Un graphe (simple) est complet si tous ses sommets sont connectés entre eux

    - Une arête peut également être **étiquetée**, c’est-à-dire se voir associer un identifiant ou un type. Par exemple, dans un graphe moléculaire, les arêtes entre deux atomes (sommets) peuvent représenter différentes liaisons chimiques avec des propriétés distinctes. Un graphe étiqueté est un graphe dont les arêtes sont étiquetées.

    - Une **chaîne** est une séquence d’arêtes consécutives dans un graphe non-orienté, dans un graphe orienté on parlera de chemin. Une chaîne ou un chemin est dit simple s’il contient au plus une arête entre deux mêmes sommets. Un graphe est **connexe** s’il existe une chaîne ou un chemin entre chaque paire de sommets.

## Structure de données

Quand on parle d'un graphe au sens mathématique, en décrivant ses propriétés, en le représentant de manière graphique, on décrit ce que l'on appelle un type abstrait. Nous l'avons déjà vu en abordant les structures linéaires (listes, piles, files).

" *En informatique, un type abstrait est une spécification mathématique d'un ensemble de données et de l'ensemble des opérations qu'on peut effectuer sur elles.* " [Page Wikipedia sur les types abstraits](https://fr.wikipedia.org/wiki/Type_abstrait)

Maintenant nous allons chercher à implémenter ce type abstrait, par une structure de données.

!!! example "Trouver une structure de données adaptée"
    Chercher plusieurs moyens de stocker un graphe en python

!!! abstract "Implémentation des graphes : Matrice d'adjacence"