# Récursivité


!!! abstract "Définition"
    **Une fonction récursive est une fonction qui contient un appel à elle même**


## Cas naïf



```python
def infini(n):
    return infini(n+1)

print(infini(0))
```
!!! help "Quel est le problème de cette fonction?"
    Boucle infinie

    ```mermaid
    graph LR;
        A["infini(0)"]-->B["infini(1)"];
        B-->C["infini(2)"];
        C-->D["infini(3)"];
        D-->E["..."]
    ```


## Cas plus élaboré

Pour que notre fonction récursive s'arrête à un moment donné, nous allons utiliser une conditionnelle

```python
def a_peu_pres_infini(n):
    if n > 500:
        return n
    else:
        return a_peu_pres_infini(n+1)

print(a_peu_pres_infini(0))
```

!!! help "Quand la fonction va-t-elle s'arrêter?"
    Quand elle aura atteint 500

## Equivalence entre boucle while et fonction récursive

Chaque fonction récursive peu s'écrire avec une boucle while et inversement

**On appelera "versions itératives" les versions avec boucle while**

```python
def a_peu_pres_infini(n):
    while n < 500:
        n = n + 1
    return n
```

Quel est donc l'intérêt des fonctions récursives?

Dans certaines situations il est plus simple et plus naturel d'écrire une fonction récursive plutôt qu'une boucle while

**Exemple avec les suites mathématiques :**

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJkAAABICAIAAABqXMBnAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAgnSURBVHhe7ZxdTBRXFMfXto8gmtAHUr8NdFGisklFRSNFwERERQ1rKh8P1gCrrS8ad32ovuiuTeybrImlcRWSghUT+Qh+Y+pHV9ytSa1QNvbBVHhgYo2sfZT+2XMZdodlZ5E7wAz3l5vLPXeY2dl75txzzszcnTU4OGgSGIKP2F+B/pnpulywYF5BQT4TdM7HJ06cYM0ZRn19XVtba2dnp8k06/379/+9e7do0SK2TZ8If2kchL80DkKXxiGWLuFREBo47HYmC6Y58JejGRgYqKqsnD//M9S9vb2sVzC9iR777LFaHz56YLMdsNsdrEsw7Ykyx7pcTiiycMtWoUh9obTLQCCwadOXaHi9nSkpKdQp0AVKu7xy5RfUMEqhSN2h1GVjYyPq9RvWkyjQEUpdSlI/6qTZSSQaBre7BgEdUiy6AdvS3Mw2GIhY+aVhQIp8/vz5VZmrHI5jcB/d3V22A9UGVGcoMxkBOSVK87VrTDYE+fl5Pp+PCYODdXWX8B2tJSVMNgozwi6BxWJhLZNp795SszkdeReTjYImurzX0QGfVF9fx+ThHr/fz+TJ5caNm6wVRnLyp6xlFDjrMhgMwjm53W74JNYVor29HT3hxjG1SJK0uWAzE4wCZ1329fV9e+hQaWkp2unpy6gTXL9xHUEHE8YJDJriT9USPhPEgP4N50micWB+cxgusY/TeQoHGRgYIBFxB0REHCSCnp6eqspKFEQlqp+F4/TGB9shJjhaZuYqgwV3hCa6hIbCo0T70aM4phxJ0mhCnWiTmsODTK3BiYVfVUaCf+wDlwnXiGSORMQ7mGDRkJ3lpUsX01LTUlNTqRMh5eXQzaZJwOVyFm0rQhzLZGPBX5cwONQZyzNQw312dT1HY93a7KFtIe513Fu8eDETTKalS5b6/D4mRIOXv4QiURtVkYC/Lkl5n5vNgUDA47mwenWWJPVvzNmITspJegI9SXNG7hEuXLRQEfQqaGq66vV2xlNi6IkUaeynePx1ef/X+6h/OHPmp9pajN1f3d0QYYt7rNbExES06ZZv/CQkJKTEB9thFFBkTc3ZO3fuwMTDCynYMPDX5YqVK+AC129Y73S5IEJ/yMphlz/W1pKPHJ2k4/9ZSwMw8UKRaMD6FQXapf8xBspn0fA6qGvOurcWFVEPd2AQ8JHuc+dIdNjtb968kUXBB8PfLlXJzc31PvYywWRC4CMel3JhCnS5a9duuMx7HR1oIz6SJGn79h20STARIuZYpBBZWV+gcfv2XfJtGoGA9vvTp+fOnYv2d8ePxwhbBPEToUvYSln5UFj/8uU/1CPQERFzbHt7O+q9Xxk2mzY2I3YZDAaXLTOjofUEK9CIEbs8cvgwaqfTJRSpU5guXS5na1uLzXbAwLcrjY+8DMioT4JmDh/5njyBRRZu2SosUu8MxT6UipjN6R7PRZHq6RcWxyJ537FjG9TZ1HQ1ISGBtgn0BYt9LBYLItju7i6KZgV6JOK+j8WSKUn9ly7WbczJYV0C/RBx32f//v2o3W43iQJ9EaHLrKw1qB8+ehAMBqlHoCMidJmWlkYNev9KoC8idClHsL2vXlFDMBFoRUZB6D1CxCLVVVWBQIBt04AIXQr4snNnsc/vKy4udjiOpaWmtba1WK0lfX19bDNvhC61Bfl6dbUN5eeGBpvtANIEj+cC28YboUut8Pv9ubm54Tdedu3ajVq7l/800eV0W385JVgsFsWr1fQwcemSpSQCvgPFWZd6WX85JZCG6KVDLQaKsy61WH85cXDhy2tOYhfYBNtHAy43Nq5bm03PozQZqKEHX2FwWbOnuv6SF8iD8VkoTB4btkJTDfmcudNx925m5ip8BJND8B0oTfwl3DsuQNnt05K88KsvfsZauoVEDenarVs3aX2BKqH1JurEeEbU0tysMOKoBXkk2yEMWOHJUycbGhrxEawrBMeBGoLpdBhcFygTsUtcZTiCbCu41nA9oofE8ZKfn8dakcjXcvhnaQ2+iyoKywM4VWtJCa0dDieegUInRgA2jTZqtO1Hj9Km0fC3S9X1lwBRADwTYja0KXJDIECb4iSGAY2Gl79ESKKKwvIQ43y9b1/RtqLRb8SpDhRmAq/3N4RCibNnu901z7ueS5LEtkWDvy5V11+O9xQnDsINxUrNsQpSe7YPD2RFRn3/RnWgthYV5eUNXVsYrqysNdXVNrSzsyOsIhz+ulRdfzneU+RCyBuqMy5zjw0psifQ4/F4YO7hhYIA1YEC1Dl/3nxYfGjtTT90T5tG8wn7y48VK1e8+PsFsii6GOX1l2Vl5fJIxThFfJPX/76mNoD54sszIYRe3mI5cvgw/bQXvh31yLx9+xZ1PAP17M9nZnM6rZ98/NiLf4j19jLzm8PA96Jo/ZMpcPhyUIMQHD6f2qMZK/aRmczYZ/IJ/9UTRD0xAh+gSU6iytPfn1ZUVFD72R/PjPcTWFzALA3LllMU+jkWl8uJfupREF2X8nytBeM9xRmL78kThLXyCwLJyclJc5IOHvxmTBfD7HMYzGmYtZCAM1kDkCdh6pATRHwiJklZVBBjjsVeOA45harKSkrCZjJKXUKLGBpVLzVpTJ8zmf4o51gEUQiWED1SIj/lyG5VoErE+7EEtFhWXgqNtra2IeVivYJpT5TYZ2NOjtPpQlZUUVHep9nLKQLuRI9jkb3WnHVLklRYuGWaTLYCVaLMsTIwSo/nQsbyDO1+t0nAkVi6FOiLid73ge3W19c57Paoz2Bj09LcXBB6D5geC+BQH3C74MP2MiAm0//mibTtGmBULgAAAABJRU5ErkJggg==)  


```python
def suite(n):
    if n == 0:
        return 2
    else:
        return -2*suite(n-1)
```

La version récursive s'écrit quasiment sans effort, tandis que la version itérative:

```python
def suite_while(n):
    Un = 2
    i = 0
    while i <= n:
        Un = -2 * Un
    
    return Un
```

Vous remarquerez que dans la version itérative, on construit le résultat à partir du cas de base (u0) pour obtenir le résultat final (un), tandis que dans la version récursive, on cherche d'abord à calculer le résultat final pour ensuite calculer le résultat un-1 etc...

## Cas de base et cas récursif


!!! abstract "Definition" 
    On appelera **cas de base** le cas de la conditionnelle dans lequel il n'y a pas d'appel à la fonction,
    
    et **cas récursif** le cas où il y a un appel à la fonction


```python
def suite(n):

    #Cas de base
    if n == 0:
        return n

    #Cas récursif
    else:
        return -2*suite(n-1)
```

!!! warning "Attention"
    Si il n'y a pas de cas de base dans votre fonction récursive, il y aura **forcément** une boucle infinie
