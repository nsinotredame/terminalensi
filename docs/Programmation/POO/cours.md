# Programmation Orientée Objet

## Histoire des langages de programmation

![](20221007145053.png)

Voici une frise chronologique sur l'évolution des langages de programmation, on note que : 

- Lisp est le premier langage fonctionnel (ancêtre de python!)
- Simula est le premier langage orienté objet

!!! info "Paradigmes"
    La programmation fonctionnelle et la programmation orientée objet sont des **paradigmes de programmation**.

    Un paradigme, pour faire simple, est une façon de voir le monde. Un paradigme de programmation est une façon de programmer.

    Il existe beaucoup de paradigmes différents, les plus importants sont la **programmation impérative**, que vous avez déjà vu depuis que vous apprennez
    python, c'est la manière de programmer qui consiste à écrire un programme ligne par ligne, qui sera exécuté ligne par ligne; mais aussi la programmation fonctionnelle, dont nous aborderons quelques aspects plus tard dans l'année.

    Nous allons donc voit un nouveau paradigme de programmation: **la programmation orientée objet**

!!! tip "Python, le langage à tout faire"
    Python est ce qu'on appelle, un langage multi-paradigmes, on peut programmer selon différents paradigmes tels que **l’impératif**, la **programmation orientée objet** ou la **programmation fonctionnelle**

## Notion de classe et d'objet

Dans la programmation orientée objet, on manipule comme son nom l’indique : des objets, et des classes.

!!! abstract "Définitions"
    **Une classe est une structure de données**, un modèle que l’on va utiliser pour créer des objets, un peu comme si on créait un type personnalisé.

    **Un objet est une instance d’une classe**, on peut créer plein d'objets indépendants à partir d'une classe

Une classe peut représenter n’importe quel concept, on peut utiliser une classe pour créer des entiers, une interface graphique, ou même d’autres concepts plus abstraits qui n’ont rien à voir avec l’informatique comme un chat.

```mermaid
graph LR;
    A["Classe: Personne <br/> Attributs: <br/> Nom <br/> Taille"];
    B["Objet : personne1 <br/> 'Clémentine' <br/> 1m70"];
    C["Objet : personne2 <br/> 'Théo' <br/> 1m80"];
    D["Objet : personne3 <br/> 'Claude' <br/> 1m75"];
    A-->B
    A-->C
    A-->D
```

!!! abstract "Contenu d'une classe"
    Une classe est caractérisée par:

    - **des attributs** : variables qui sont propres à chaque objet instancié
    - **des méthodes** : fonctions renvoyant des informations sur l’objet, et/ou modifiant l’objet

!!! warning "Vocabulaire spécifique"

    Lorsque l’on est dans un contexte de programmation objet, le vocabulaire change. 

    - **Variable** appartenant à une classe ? → **attribut**
    - **Fonction** appartenant à une classe ? → **méthode**

## Syntaxe en python

!!! abstract "Création d'une classe en python:"
    ```python
    class Personne:
        #Constructeur:
        def __init__(self,taille,nom):
            #Attributs:
            self.taille = taille
            self.nom = nom

        #Methodes:
        def fait_moins_de_2m(self):
            return self.taille < 200
    ```
    !!! tip "Constructeur"
        La méthode `__init__` est une méthode appellée constructeur, elle est appellée à chaque création d'une instance de cette classe.

        L'argument `self` dans les méthodes représente l'objet courant

!!! abstract "Création d'un objet et accès aux attributs/méthodes:"
    Création d'un objet `personne` à partir de la classe `Personne`, 210 et `Aristote` sont les arguments du constructeur :

    ```python
    personne = Personne(210, "Aristote")
    ```

    Accès à l'attribut `nom` de l'objet `personne`:

    ![](20221009181234.png)

    Accès à la méthode `fait_moins_de_2m()` de l'objet `personne`:

    ![](20221009181414.png)