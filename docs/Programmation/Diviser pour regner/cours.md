# Diviser pour régner

!!! abstract "Principe"
    Le principe de **diviser pour régner** est une technique en algorithmique consistant à diviser un problemes en sous problemes pour le résoudre plus rapidement. Un algorithme appliquant la technique **diviser pour régner** va généralement suivre ces 3 étapes:

    - Diviser : on découpe le probleme initial en sous problemes de la plus petite taille possible.
    - Régner : on résout les sous problemes
    - Combiner : les solutions des sous problemes sont assemblées pour former la solution au probleme initial

!!! tip "Avantages"
    Diviser pour régner permet de résoudre des problemes d'apparence difficile de manière simple, on divise le problème initial en sous problème car il est plus simple de résoudre le probleme avec une donnée de petite taille.

    - Exemple avec le tri fusion: il est très simple de trier une liste de 2 éléments

    On gagne aussi en temps de calcul, puisque diviser en problèmes triviaux permet de ne pas faire de calculs superflux. Aussi les ordinateurs modernes étant de plus en plus capables de [parallélisme](https://fr.wikipedia.org/wiki/Parall%C3%A9lisme_(informatique)), et donc étant capables de faire plusieurs calculs en même temps, on peut calculer les solutions à tous les sous problemes simultanéments.


## Exemple : le tri fusion

Vous avez vu l'année dernière deux algorithmes de tri : le tri par sélection, et le tri par insertion.

Ces deux algorithmes avaient un cout en moyenne de l'ordre de O(N²)

Mais il existe des algorithmes qui ont un coût de l'ordre de O(N*log(N)) qui est beaucoup moins élevé! Mais comment peut on atteindre cette complexité? Comment faire pour économiser des opérations par rapport au tri par sélection?

Nous allons justement appliquer le principe de diviser pour régner.

!!! abstract "Principe de l'algorithme"
    On applique les différentes étapes de la technique diviser pour régner : 

    |Diviser|Regner|Combiner|
    |--|--|--|
    |on découpe le tableau T[1, .. n] à trier en deux sous-tableaux T[1, .. n/2] et T[n/2 +1,..n] | on trie les deux sous-tableaux T[1, .. n/2] et T[n/2 +1,..n] (récursivement, ou on ne fait rien s'ils sont de taille 1) | on calcule une permutation triée du tableau initial en fusionnant les deux sous-tableaux triés T[1, .. n/2] et T[n/2 +1,..n]. |

    Illustration : 
    ![Animation tri fusion](https://upload.wikimedia.org/wikipedia/commons/c/cc/Merge-sort-example-300px.gif)

    source : [page wikipedia sur le tri fusion](https://en.wikipedia.org/wiki/Merge_sort) 