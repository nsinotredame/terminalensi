# Cours de Terminale NSI

Site des cours de terminale NSI au lycée Notre Dame à Rezé

[sujets de bac des années précédentes](https://kxs.fr/sujets/terminale-ecrit)


## Advent of code 2022

Vous pouvez participer à l'advent of code et vous créer un leaderboard privé : https://adventofcode.com/

!!! info "Jour 5"

    Le puzzle du jour 5 est un exercice parfait sur les piles
    Voici le [code pour le parsing](data/aoc_jour_5_eleve.py), vous pouvez vous créer un compte et avoir votre propre entrée, sinon voici [mon entrée](data/input), à vous de trouver la même réponse 

Vidéo à regarder pour la rentrée : <iframe width="560" height="315" src="https://www.youtube.com/embed/Fqvo6M2d5IQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Vidéos intéressantes

Vidéo sur le HTTPS : 

<iframe width="560" height="315" src="https://www.youtube.com/embed/Fqvo6M2d5IQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

!!! info "Histoire des machines à calculer"

    Série de vidéos sur les machines à calculer

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GnMgHsos7cY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/vOGtYgu7V7Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/zwlZkM3Xu4U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Environnement python de secours


!!! tip "Environnement dans le navigateur" 
    Vous pouvez utiliser cet environnement python dans votre navigateur au cas où vous n'avez pas de quoi faire du python à la maison
    {{IDE()}}

Ou ici si votre code est trop conséquent : [Try jupyter](https://jupyter.org/try-jupyter/retro/notebooks/?path=notebooks/Intro.ipynb)

!!! tip "Exemples de sujets de grand oral"
    === "Sujets grand oral"
        Voici une liste de sujets de grand oral dont vous pouvez vous inspirer
    
    === "Voir la liste"
        Comment l’informatique permet-elle de sécuriser les transactions des cryptomonaies ?

        Qu’est-ce qui fait qu’une voiture est autonome ?

        Comment les robots militaires sont-ils contrôlés ?

        Quels sont les impacts de l’obsolescence programmée dans la société ?

        Comment le machine learning a-t-il révolutionné le domaine du médical

        En quoi les IA et les robots ont-ils permit une hausse de production dans les entreprises ?

        Comment l’informatique permet d’améliorer l’aide humanitaire?

        En quoi les programmes écrits en langage en python permettent-ils d’effectuer des calculs 
        scientifiques complexes ?

        En quoi les bases de données permettent la recherche d’information ?

        En quoi la cryptographie peut-elle optimiser la sécurisation des informations confidentielles?

        Par quelles mesures la technologie Blockchain va t-elle remplacer les transactions classique
        En quoi la miniaturisation a permis l’évolution des ordinateurs ?

        Quels sont les impacts de l’avancée des systèmes informatiques sur les emplois ?

        En quoi les bases de données ont permis une évolution de l’organisation du travail ?

        En quoi les nouvelles technologies ont impactés l’engagement politique ?

        De quelle manière sont cryptées les données informatiques ?

        Quel est le role de l’informatique dans la sécurité intérieure ?

        En quoi les bases de données sont-elles importantes dans la prise de décision ?

        Comment améliorer ses performances sportive grâce à l’informatique ?

        La 5G: quels sont ses avantages et ses inconvénients ?

        Comment fonctionne un logiciel de modélisation tridimentionnelle ?

        Quel impact les IA ont elles eu dans le monde des échecs ?

        Quels sont les impacts et la dangerosité des bugs dans notre société ?
        
        Comment un ordinateur peut-il apprendre de ses erreurs et pourquoi ?
        
        En quoi la notion de récurrence intervient-elle dans l’élaboration et le fonctionnement d’un 
        programme récursif
        
        Quelle est l’importance des graphes de nos jours ?
        
        En quoi le raisonnement par récurrence est-il utile en mathématiques et informatiques ?
        
        A quoi servent les bases de données ?
        
        Comment les bases de données ont-elles révolutionné notre quotidien ?
        
        Pourquoi chiffrer les communications ?
        
        Comment le cryptage nous permet-il d’utiliser les outils informatiques de manière sûre ?
        
        Quelles sont les différentes étapes pour parvenir à un site web complet ?
        
        Quels sont les impacts et la dangerosité des bugs dans notre société
        
        Comment les transactions des cryptomonnaies sont-elles sécurisés ?
        
        Comment calculer le coût d’exécution en temps d’un algorithme ?
        
        Quels sont les les enjeux du cyber espace et quel est le rôle de la cryptologie dans cette 
        nouvelle plateforme ?
        
        Pourquoi utilise ton les nombres premiers dans l’algorithme RSA ?

        Comment s’adapter à la cyberguerre ?

        Comment casser le chiffrement de Vigenère (Maths / NSI)

        Comment les cryptomonnaies sont-elles sécurisées ?

        Comment l’art de couper en deux permet-il de résoudre des problèmes en mathématiques et informatique (Maths/NSI)

        Les tours de Hanoï , entre récurrence et récursivité (Maths/NSI)

        Comment les caractères de nos claviers sont-ils encodés ?

        Deep Blue, un algorithme imbattable ? (parle d’IA et de bases de données)

        Que se cache-t-il derrière une page web ?

        La suite de Fibonacci, quels algorithmes pour calculer ses termes? (Maths/NSI)

        Comment les données peuvent elle aider les entreprises à se développer ?

        Comment les systèmes d’exploitation permettent de rendre aisée l’utilisation de notre ordinateur ?

        Comment l’algèbre booléenne a-t-elle permis le développement de l’informatique ?
        
        Récursivité et récurrence : similarités et différences.
        
        La sonde Perseverance et le binaire
        
        Bug de rover martien et système d’exploitation multitâche
        
        Encoder toutes les écritures du monde avec des 1 et des 0
        
        Fonctionnement et enjeux des BDD
